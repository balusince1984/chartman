package com.taletellers.chartman.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClientAccount.
 */
@Entity
@Table(name = "cm_client_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "account_id")
	private Long id;

	@NotNull
	@Size(min = 3, max = 200)
	@Column(name = "account_name", length = 200, nullable = false)
	private String accountName;

	@Size(max = 1000)
	@Column(name = "account_description", length = 1000)
	private String accountDescription;

	@Column(name = "account_status")
	private Boolean accountStatus;

	@OneToOne
	private Organisation organisation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountName() {
		return accountName;
	}

	public ClientAccount accountName(String accountName) {
		this.accountName = accountName;
		return this;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public ClientAccount accountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
		return this;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public Boolean isAccountStatus() {
		return accountStatus;
	}

	public ClientAccount accountStatus(Boolean accountStatus) {
		this.accountStatus = accountStatus;
		return this;
	}

	public void setAccountStatus(Boolean accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public ClientAccount organisation(Organisation organisation) {
		this.organisation = organisation;
		return this;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ClientAccount clientAccount = (ClientAccount) o;
		if (clientAccount.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, clientAccount.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClientAccount{" + "id=" + id + ", accountName='" + accountName + "'" + ", accountDescription='"
				+ accountDescription + "'" + ", accountStatus='" + accountStatus + "'" + '}';
	}
}
