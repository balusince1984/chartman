package com.taletellers.chartman.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClientApplication.
 */
@Entity
@Table(name = "cm_client_application")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientApplication implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "app_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Size(min = 3, max = 200)
	@Column(name = "app_name", length = 200, nullable = false)
	private String applicationName;

	@Size(max = 1000)
	@Column(name = "app_desc", length = 1000)
	private String applicationDescription;

	@Column(name = "active")
	private Boolean active;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id")
	private ClientAccount clientAccount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public ClientApplication applicationName(String applicationName) {
		this.applicationName = applicationName;
		return this;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationDescription() {
		return applicationDescription;
	}

	public ClientApplication applicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
		return this;
	}

	public void setApplicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
	}

	public Boolean isActive() {
		return active;
	}

	public ClientApplication active(Boolean active) {		
		this.active = active;
		return this;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ClientAccount getClientAccount() {
		return clientAccount;
	}

	public ClientApplication clientAccount(ClientAccount ClientAccount) {
		this.clientAccount = ClientAccount;
		return this;
	}

	public void setClientAccount(ClientAccount ClientAccount) {
		this.clientAccount = ClientAccount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ClientApplication clientApplication = (ClientApplication) o;
		if (clientApplication.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, clientApplication.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClientApplication{" + "id=" + id + ", applicationName='" + applicationName + "'"
				+ ", applicationDescription='" + applicationDescription + "'" + ", active='" + active + "'" + '}';
	}
}
