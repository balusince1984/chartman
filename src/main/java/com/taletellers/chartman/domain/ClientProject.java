package com.taletellers.chartman.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClientProject.
 */
@Entity
@Table(name = "cm_client_project")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientProject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "project_id")
    private Long id;

    @NotNull
    @Size(min = 3, max = 200)
    @Column(name = "project_name", length = 200, nullable = false)
    private String projectName;

    @Size(max = 1000)
    @Column(name = "project_desc", length = 1000)
    private String projectDescription;

    @Column(name = "active")
    private Boolean active;
    
    @OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "app_id")
	private ClientApplication clientApplication;

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public ClientProject projectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public ClientProject projectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
        return this;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public Boolean isActive() {
        return active;
    }
    
    public ClientApplication getClientApplication() {
		return clientApplication;
	}

	public void setClientApplication(ClientApplication clientApplication) {
		this.clientApplication = clientApplication;
	}

    public ClientProject active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientProject clientProject = (ClientProject) o;
        if (clientProject.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, clientProject.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ClientProject{" +
            "id=" + id +
            ", projectName='" + projectName + "'" +
            ", projectDescription='" + projectDescription + "'" +
            ", active='" + active + "'" +
            '}';
    }
}
