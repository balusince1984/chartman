package com.taletellers.chartman.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClientTest.
 */
@Entity
@Table(name = "cm_client_test")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientTest implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "test_id")
	private Long id;

	@NotNull
	@Size(min = 3, max = 200)
	@Column(name = "test_name", length = 200, nullable = false)
	private String testName;

	@Size(max = 1000)
	@Column(name = "test_description", length = 1000)
	private String testDescription;

	@Column(name = "test_date")
	private ZonedDateTime testDate;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "test_key", updatable = false)
	private String testKey;

	@Column(name = "test_status")
	@Enumerated(EnumType.STRING)
	private ClientTestStatus clientTestStatus;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "project_id")
	private ClientProject clientProject;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTestName() {
		return testName;
	}

	public ClientTest testName(String testName) {
		this.testName = testName;
		return this;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getTestDescription() {
		return testDescription;
	}

	public ClientTest testDescription(String testDescription) {
		this.testDescription = testDescription;
		return this;
	}

	public void setTestDescription(String testDescription) {
		this.testDescription = testDescription;
	}

	public ZonedDateTime getTestDate() {
		return testDate;
	}

	public ClientTest testDate(ZonedDateTime testDate) {
		this.testDate = testDate;
		return this;
	}

	public void setTestDate(ZonedDateTime testDate) {
		this.testDate = testDate;
	}

	public Boolean isActive() {
		return active;
	}

	public ClientTest active(Boolean active) {
		this.active = active;
		return this;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ClientProject getClientProject() {
		return clientProject;
	}

	public void setClientProject(ClientProject clientProject) {
		this.clientProject = clientProject;
	}

	public String getTestKey() {
		return testKey;
	}

	public void setTestKey(String testKey) {
		this.testKey = testKey;
	}

	public ClientTestStatus getClientTestStatus() {
		return clientTestStatus;
	}

	public void setClientTestStatus(ClientTestStatus clientTestStatus) {
		this.clientTestStatus = clientTestStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ClientTest clientTest = (ClientTest) o;
		if (clientTest.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, clientTest.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClientTest{" + "id=" + id + ", testName='" + testName + "'" + ", testDescription='" + testDescription
				+ "'" + ", testDate='" + testDate + "'" + ", active='" + active + "'" + '}';
	}

}
