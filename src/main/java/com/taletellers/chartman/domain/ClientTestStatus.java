package com.taletellers.chartman.domain;

public enum ClientTestStatus {
	RUNNING, FINISHED
}
