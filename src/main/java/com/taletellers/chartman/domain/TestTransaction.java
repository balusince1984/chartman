package com.taletellers.chartman.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A TestTransaction.
 */
@Entity
@Table(name = "cm_test_transactions")
@IdClass(TestTransactionKey.class)
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TestTransaction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "transaction_code")
	private String id;

	@Column(name = "transaction_name")
	private String name;

	@Column(name = "transaction_desc")
	private String description;

	@Column(name = "transaction_notes")
	private String notes;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "test_id")
	@Id
	private ClientTest clientTest;

	public String getId() {
		return id;
	}

	public TestTransaction id(String id) {
		this.id = id;
		return this;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public TestTransaction name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public TestTransaction description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public TestTransaction notes(String notes) {
		this.notes = notes;
		return this;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public ClientTest getClientTest() {
		return clientTest;
	}

	public void setClientTest(ClientTest clientTest) {
		this.clientTest = clientTest;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TestTransaction testTransaction = (TestTransaction) o;
		if (testTransaction.getId() == null || id == null) {
			return false;
		}
		return Objects.equals(id, testTransaction.getId())
				&& Objects.equals(clientTest.getId(), testTransaction.getClientTest().getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TestTransaction{id='" + id + "'" + ", name='" + name + "'" + ", description='" + description + "'"
				+ ", notes='" + notes + "'" + '}';
	}

}
