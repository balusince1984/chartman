package com.taletellers.chartman.domain;

import java.io.Serializable;

public class TestTransactionKey implements Serializable {

	private static final long serialVersionUID = 7785686769864053016L;

	private String id;
	private ClientTest clientTest;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getClientTest() {
		return clientTest.getId();
	}

	public void setClientTest(ClientTest clientTest) {
		this.clientTest = clientTest;
	}


}
