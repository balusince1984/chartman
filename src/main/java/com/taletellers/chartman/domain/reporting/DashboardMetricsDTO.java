package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class DashboardMetricsDTO  implements Serializable {

	
	private static final long serialVersionUID = 8190149585276979006L;
	
	private int userLoad;
	
	private double avgResponseTime;
	
	private double hitsPerSecond;
	
	private double throughput;
	
	private double errorsPerSecond;

	public int getUserload() {
		return userLoad;
	}

	public void setUserload(int userload) {
		this.userLoad = userload;
	}

	public double getAvgResponseTime() {
		return avgResponseTime;
	}

	public void setAvgResponseTime(double avgresponsetime) {
		this.avgResponseTime = avgresponsetime;
	}

	public double getHitsPerSecond() {
		return hitsPerSecond;
	}

	public void setHitsPerSecond(double hitspersecond) {
		this.hitsPerSecond = hitspersecond;
	}

	public double getThroughput() {
		return throughput;
	}

	public void setThroughput(double throughput) {
		this.throughput = throughput;
	}

	public double getErrorsPerSecond() {
		return errorsPerSecond;
	}

	public void setErrorsPerSecond(double errorspersecond) {
		this.errorsPerSecond = errorspersecond;
	}
	
	
}
