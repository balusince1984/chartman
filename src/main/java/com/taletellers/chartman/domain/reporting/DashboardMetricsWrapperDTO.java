package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;
import com.taletellers.chartman.domain.reporting.DashboardMetricsDTO;
import java.util.List;
public class DashboardMetricsWrapperDTO  implements Serializable {

	
	private static final long serialVersionUID = -1074827440381733732L;

	private DashboardMetricsDTO dashboardMetrics;
	
	private List<HttpResponseCodeDistributionDTO> httpResponseDistributionList;
	
	private ResponseTimeSlaDistributionDTO responseTimeSlaDistribution;
	
	private TransactionStatusCountDTO transactionStatusCount;

	private List<ResponseTimeSummaryDTO> slowResponseTimeSummaryList;
	
	private List<ResponseTimeDTO> responseTimeList;

	private TransResponseTimeDistributionDTO transResponseTimeDistribution;
	
	public DashboardMetricsDTO getDashboardMetrics() {
		return dashboardMetrics;
	}

	public void setDashboardMetrics(DashboardMetricsDTO dashboardmetrics) {
		this.dashboardMetrics = dashboardmetrics;
	}

	
	/*public HttpResponseCodeDistributionDTO getHttpResponseDistribution() {
		return httpResponseDistribution;
	}

	public void setHttpResponseDistribution(HttpResponseCodeDistributionDTO httpresponsedistribution) {
		this.httpResponseDistribution = httpresponsedistribution;
	}*/

	public List<HttpResponseCodeDistributionDTO> getHttpResponseDistributionList() {
		return httpResponseDistributionList;
	}

	public void setHttpResponseDistributionList(List<HttpResponseCodeDistributionDTO> httpResponseDistributionList) {
		this.httpResponseDistributionList = httpResponseDistributionList;
	}

	public ResponseTimeSlaDistributionDTO getResponseTimeSlaDistribution() {
		return responseTimeSlaDistribution;
	}

	public void setResponseTimeSlaDistribution(ResponseTimeSlaDistributionDTO responsetimesladistribution) {
		this.responseTimeSlaDistribution = responsetimesladistribution;
	}

	public TransactionStatusCountDTO getTransactionStatusCount() {
		return transactionStatusCount;
	}

	public void setTransactionStatusCount(TransactionStatusCountDTO transactionstatuscount) {
		this.transactionStatusCount = transactionstatuscount;
	}

	public List<ResponseTimeSummaryDTO> getSlowResponseTimeSummaryList() {
		return slowResponseTimeSummaryList;
	}

	public void setSlowResponseTimeSummaryList(List<ResponseTimeSummaryDTO> slowResponseTimeSummaryList) {
		this.slowResponseTimeSummaryList = slowResponseTimeSummaryList;
	}

	/*public ResponseTimeSummaryDTO getSlowResponseTimeSummaryList() {
		return List<ResponseTimeSummaryDTO>;
	}

	public void setSlowResponseTimeSummary(ResponseTimeSummaryDTO slowresponsetimesummary) {
		this.slowResponseTimeSummaryList = slowresponsetimesummary;
	}
*/
	public List<ResponseTimeDTO> getResponseTimeList() {
		return responseTimeList;
	}

	public void getResponseTimeList(List<ResponseTimeDTO> responseTimeList) {
		this.responseTimeList = responseTimeList;
	}

	public TransResponseTimeDistributionDTO getTransResponseTimeDistribution() {
		return transResponseTimeDistribution;
	}

	public void setTransResponseTimeDistribution(TransResponseTimeDistributionDTO transResponseTimeDistribution) {
		this.transResponseTimeDistribution = transResponseTimeDistribution;
	}

	public void setResponseTimeList(List<ResponseTimeDTO> responseTimeList) {
		this.responseTimeList = responseTimeList;
	}
	
	
	
	
}
