package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ExecSummaryDTO implements Serializable {

	private static final long serialVersionUID = 867824425900814011L;

	private int time;

	private int load;

	private double value;


	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getLoad() {
		return load;
	}

	public void setLoad(int load) {
		this.load = load;
	}

}
