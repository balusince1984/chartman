package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class HttpResponseCodeDistributionDTO implements Serializable {

	private static final long serialVersionUID = 202675712723459466L;
	
	private String httpResponseCode;
	
	private int count;
	
	public String getHttpResponseCode() {
		return httpResponseCode;
	}
	public void setHttpResponseCode(String httpresponsecode) {
		this.httpResponseCode = httpresponsecode;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
