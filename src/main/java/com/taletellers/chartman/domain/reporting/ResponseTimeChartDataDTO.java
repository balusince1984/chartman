package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ResponseTimeChartDataDTO implements Serializable {

	private static final long serialVersionUID = 2624190237879790498L;

	private String elapsedTime;
	
	private double responseTime;
	
	private int allThreads;
	
	public String getElapsedTime() {
		return elapsedTime;
	}
	
	public void setElapsedTime(String elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	
	public double getResponseTime() {
		return responseTime;
	}
	
	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}
	
	public int getAllThreads() {
		return allThreads;
	}
	
	public void setAllThreads(int allThreads) {
		this.allThreads = allThreads;
	}
	
	
}
