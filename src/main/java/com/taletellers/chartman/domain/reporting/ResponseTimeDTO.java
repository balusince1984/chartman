package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ResponseTimeDTO implements Serializable {

	private static final long serialVersionUID = 2537409182881626466L;

	private int timeIndex;

	private double responseTime;

	/*private int allThreads;*/

	public int getTimeIndex() {
		return timeIndex;
	}

	public void setTimeIndex(int timeIndex) {
		this.timeIndex = timeIndex;
	}

	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

/*	public int getAllThreads() {
		return allThreads;
	}

	public void setAllThreads(int allThreads) {
		this.allThreads = allThreads;
	}*/

}
