package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ResponseTimeSlaDistributionDTO implements Serializable {

	
	private static final long serialVersionUID = -3031915669465391903L;

	private int below;
	
	private int above;

	public int getBelow() {
		return below;
	}

	public void setBelow(int below) {
		this.below = below;
	}

	public int getAbove() {
		return above;
	}

	public void setAbove(int above) {
		this.above = above;
	}
	
	
}
