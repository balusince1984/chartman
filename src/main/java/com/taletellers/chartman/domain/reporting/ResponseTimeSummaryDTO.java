package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ResponseTimeSummaryDTO implements Serializable {

	private static final long serialVersionUID = 8048404031587197305L;
	
	private String transactioncode;
	private double minresponsetime;
	private double avgresponsetime;
	private double maxresponsetime;
	private double stddeviation;
	private double percentile;
	private int totalcount;
	private int successcount;
	private int failedcount;
	
	public String getTransactioncode() {
		return transactioncode;
	}
	public void setTransactioncode(String transactioncode) {
		this.transactioncode = transactioncode;
	}
	public double getMinresponsetime() {
		return minresponsetime;
	}
	public void setMinresponsetime(double minresponsetime) {
		this.minresponsetime = minresponsetime;
	}
	public double getAvgresponsetime() {
		return avgresponsetime;
	}
	public void setAvgresponsetime(double avgresponsetime) {
		this.avgresponsetime = avgresponsetime;
	}
	public double getMaxresponsetime() {
		return maxresponsetime;
	}
	public void setMaxresponsetime(double maxresponsetime) {
		this.maxresponsetime = maxresponsetime;
	}
	public double getStddeviation() {
		return stddeviation;
	}
	public void setStddeviation(double stddeviation) {
		this.stddeviation = stddeviation;
	}
	public double getPercentile() {
		return percentile;
	}
	public void setPercentile(double percentile) {
		this.percentile = percentile;
	}
	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	public int getSuccesscount() {
		return successcount;
	}
	public void setSuccesscount(int successcount) {
		this.successcount = successcount;
	}
	public int getFailedcount() {
		return failedcount;
	}
	public void setFailedcount(int failedcount) {
		this.failedcount = failedcount;
	}
	
	
}
