package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class ResponseTimeTrendDataDTO implements Serializable {

	private static final long serialVersionUID = -1121580944756242760L;

	private String responseTime;

	private String userLoad;

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getUserLoad() {
		return userLoad;
	}

	public void setUserLoad(String userLoad) {
		this.userLoad = userLoad;
	}

}
