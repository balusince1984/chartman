package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

/**
 * @author rponnala
 *
 */
public class TransResponseTimeDistributionDTO implements Serializable {

	
	private static final long serialVersionUID = -3031915669465391903L;

	private int rangeOne;
	
	private int rangeTwo;

	private int rangeThree;

	public int getRangeOne() {
		return rangeOne;
	}

	public void setRangeOne(int rangeOne) {
		this.rangeOne = rangeOne;
	}

	public int getRangeTwo() {
		return rangeTwo;
	}

	public void setRangeTwo(int rangeTwo) {
		this.rangeTwo = rangeTwo;
	}

	public int getRangeThree() {
		return rangeThree;
	}

	public void setRangeThree(int rangeThree) {
		this.rangeThree = rangeThree;
	}
	
	
}
