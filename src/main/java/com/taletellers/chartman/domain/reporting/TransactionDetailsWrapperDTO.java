package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;
import java.util.List;

public class TransactionDetailsWrapperDTO implements Serializable {

	
	private static final long serialVersionUID = -208480620810887246L;
	
	private List<HttpResponseCodeDistributionDTO> httpResponseDistributionList;
	
	//private List<UserLoadDTO> userLoad;

	//private List<ResponseTimeDTO> transactionTrendData;

	private ResponseTimeSummaryDTO transactionSummary;
	
	private ResponseTimeSlaDistributionDTO responseTimeSlaDistribution;
	
	private TransactionStatusCountDTO transactionStatusCount;

	private TransResponseTimeDistributionDTO transResponseTimeDistribution;
	
	

	/*public List<UserLoadDTO> getUserLoad() {
		return userLoad;
	}

	public void setUserLoad(List<UserLoadDTO> userLoad) {
		this.userLoad = userLoad;
	}*/
	
	public ResponseTimeSummaryDTO getTransactionSummary() {
		return transactionSummary;
	}

	public void setTransactionSummary(ResponseTimeSummaryDTO transactionSummary) {
		this.transactionSummary = transactionSummary;
	}

	public List<HttpResponseCodeDistributionDTO> getHttpResponseDistributionList() {
		return httpResponseDistributionList;
	}

	public void setHttpResponseDistributionList(List<HttpResponseCodeDistributionDTO> httpResponseDistributionList) {
		this.httpResponseDistributionList = httpResponseDistributionList;
	}

	public ResponseTimeSlaDistributionDTO getResponseTimeSlaDistribution() {
		return responseTimeSlaDistribution;
	}

	public void setResponseTimeSlaDistribution(ResponseTimeSlaDistributionDTO responsetimesladistribution) {
		this.responseTimeSlaDistribution = responsetimesladistribution;
	}

	public TransactionStatusCountDTO getTransactionStatusCount() {
		return transactionStatusCount;
	}

	public void setTransactionStatusCount(TransactionStatusCountDTO transactionstatuscount) {
		this.transactionStatusCount = transactionstatuscount;
	}

	

/*	public List<ResponseTimeDTO> getTransactionTrendData() {
		return transactionTrendData;
	}

	public void setTransactionTrendData(List<ResponseTimeDTO> transactionTrendData) {
		this.transactionTrendData = transactionTrendData;
	}
*/
	public TransResponseTimeDistributionDTO getTransResponseTimeDistribution() {
		return transResponseTimeDistribution;
	}

	public void setTransResponseTimeDistribution(TransResponseTimeDistributionDTO transResponseTimeDistribution) {
		this.transResponseTimeDistribution = transResponseTimeDistribution;
	}

			
		
}
