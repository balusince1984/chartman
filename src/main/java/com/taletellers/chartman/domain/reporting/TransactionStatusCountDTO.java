package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class TransactionStatusCountDTO implements Serializable {

	private static final long serialVersionUID = -5822264060295173828L;

	private int totalCount;
	
	private int successCount;
	
	private int failedCount;
	
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalcount) {
		this.totalCount = totalcount;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successcount) {
		this.successCount = successcount;
	}
	public int getFailedCount() {
		return failedCount;
	}
	public void setFailedCount(int failedcount) {
		this.failedCount = failedcount;
	}
	
}
