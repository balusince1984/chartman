package com.taletellers.chartman.domain.reporting;

import java.io.Serializable;

public class UserLoadDTO implements Serializable {

	private static final long serialVersionUID = 2537409182881626466L;

	private int timeIndex;

	private int allThreads;

	public int getTimeIndex() {
		return timeIndex;
	}

	public void setTimeIndex(int timeIndex) {
		this.timeIndex = timeIndex;
	}

	public int getAllThreads() {
		return allThreads;
	}

	public void setAllThreads(int allThreads) {
		this.allThreads = allThreads;
	}

}
