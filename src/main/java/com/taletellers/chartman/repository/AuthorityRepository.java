package com.taletellers.chartman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.Authority;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
