package com.taletellers.chartman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.ClientAccount;

/**
 * Spring Data JPA repository for the ClientAccount entity.
 */
@SuppressWarnings("unused")
public interface ClientAccountRepository extends JpaRepository<ClientAccount,Long> {

}
