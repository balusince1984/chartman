package com.taletellers.chartman.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.ClientApplication;

/**
 * Spring Data JPA repository for the ClientApplication entity.
 */
public interface ClientApplicationRepository extends JpaRepository<ClientApplication, Long> {

	Page<ClientApplication> findByClientAccountId(Long accountId, Pageable pageable);

}
