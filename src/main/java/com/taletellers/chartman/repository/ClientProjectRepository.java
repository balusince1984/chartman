package com.taletellers.chartman.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.ClientProject;

/**
 * Spring Data JPA repository for the ClientProject entity.
 */
@SuppressWarnings("unused")
public interface ClientProjectRepository extends JpaRepository<ClientProject, Long> {

	Page<ClientProject> findByClientApplicationId(Long applicationId, Pageable pageable);

}
