package com.taletellers.chartman.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.ClientTest;

/**
 * Spring Data JPA repository for the ClientTest entity.
 */
public interface ClientTestRepository extends JpaRepository<ClientTest, Long> {

	Page<ClientTest> findByClientProjectId(Long projectId, Pageable pageable);

}
