package com.taletellers.chartman.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.taletellers.chartman.domain.Organisation;

/**
 * Spring Data JPA repository for the Organisation entity.
 */
@SuppressWarnings("unused")
public interface OrganisationRepository extends JpaRepository<Organisation,Long> {

}
