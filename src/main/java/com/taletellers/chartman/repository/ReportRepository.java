package com.taletellers.chartman.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.taletellers.chartman.domain.ClientTest;
import com.taletellers.chartman.domain.TestTransaction;
import com.taletellers.chartman.domain.reporting.DashboardMetricsDTO;
import com.taletellers.chartman.domain.reporting.ExecSummaryDTO;
import com.taletellers.chartman.domain.reporting.HttpResponseCodeDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeChartDataDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSlaDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSummaryDTO;
import com.taletellers.chartman.domain.reporting.TransResponseTimeDistributionDTO;
import com.taletellers.chartman.domain.reporting.TransactionStatusCountDTO;
import com.taletellers.chartman.domain.reporting.UserLoadDTO;
import com.taletellers.chartman.web.rest.ReportingResource;

@Repository
public class ReportRepository {

	private final NamedParameterJdbcTemplate jdbcTemplate;

	private final Logger log = LoggerFactory.getLogger(ReportRepository.class);

	private final String LOAD_RESPONSE_TIME = "SELECT response_values FROM `cm_test_transactions` WHERE transaction_code = :transaction_code AND test_id = :test_id";

	/*1*/
	
	private final String LOAD_USER_LOAD = "SELECT user_load FROM `cm_client_test` WHERE test_id = :test_id";

	private final String LOAD_HITS_PER_SECOND = "SELECT SUM(sample_count)/10 as value,MAX(all_threads) AS all_threads,time_n_stage FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' GROUP BY time_n_stage";

	private final String LOAD_THROUGHPUT = "SELECT SUM(bytes)/10 as value,MAX(all_threads) AS all_threads ,time_n_stage FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' GROUP BY time_n_stage";

	private final String LOAD_ERRORS_PER_SECOND = "SELECT SUM(error_count)/10 as value,MAX(all_threads) AS all_threads,time_n_stage FROM cm_test_results WHERE test_id = :test_id GROUP BY time_n_stage";

	private final String LOAD_AVERAGE_RESPONSE_TIME = "SELECT AVG(response_time)/1000 as value,MAX(all_threads) AS all_threads,time_n_stage FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' GROUP BY time_n_stage";

	/*1*/
	
	private final String RESPONSE_TIME_SUMMARY = "SELECT transaction_code, CAST((min_response_time/1000) AS DECIMAL (12,2)) AS minresponsetime, CAST((avg_response_time/1000) AS DECIMAL (12,2)) AS avgresponsetime, CAST((max_response_time/1000) AS DECIMAL (12,2)) AS maxresponsetime, CAST((percentile/1000) AS DECIMAL (12,2)) AS percentile, CAST((std_deviation/1000) AS DECIMAL (12,2)) AS stddeviation, sample_count AS totalcount, (sample_count - error_count) AS successcount, error_count AS failedcount FROM cm_test_transactions  WHERE test_id = :test_id ORDER BY transaction_code";

	private final String SLOW_RESPONSE_TIME_SUMMARY = "SELECT transaction_code, CAST((min_response_time/1000) AS DECIMAL (12,2)) AS minresponsetime, CAST((avg_response_time/1000) AS DECIMAL (12,2)) AS avgresponsetime, CAST((max_response_time/1000) AS DECIMAL (12,2)) AS maxresponsetime, CAST((percentile/1000) AS DECIMAL (12,2)) AS percentile, CAST((std_deviation/1000) AS DECIMAL (12,2)) AS stddeviation, sample_count AS totalcount, (sample_count - error_count) AS successcount, error_count AS failedcount FROM cm_test_transactions  WHERE test_id = :test_id ORDER BY avg_response_time/1000 DESC LIMIT 0,5";

	/*2*/
	private final String CURR_USER_LOAD = "SELECT DISTINCT all_threads FROM cm_test_results WHERE test_id = :test_id ORDER BY time_index DESC LIMIT 1";
	/*2*/
	
	/*3*/
	private final String CURR_HITS_PER_SECOND = "SELECT CAST(SUM(sample_count)/10  AS DECIMAL(12,2)) AS VALUE FROM cm_test_results WHERE test_id=:test_id AND success_status = 'true' GROUP BY time_n_stage ORDER BY time_n_stage DESC LIMIT 1";

	private final String CURR_THROUGHPUT_KBPS = "SELECT CAST(SUM(bytes)/10240  AS DECIMAL(12,2)) AS VALUE FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' GROUP BY time_n_stage ORDER BY time_n_stage DESC LIMIT 1";

	private final String CURR_ERRORS_PER_SECOND = "SELECT CAST(SUM(error_count)/10  AS DECIMAL(12,2)) AS VALUE FROM cm_test_results WHERE test_id = :test_id GROUP BY time_n_stage ORDER BY time_n_stage DESC LIMIT 1";

	private final String CURR_OVERALL_RESPONSE_TIME = "SELECT CAST(AVG(response_time/1000) AS DECIMAL(12,2)) AS VALUE FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true'";

	/*3*/
	
	
	private final String TRANSACTION_STATUS_DISTRIBUTION = "SELECT SUM(sample_count) AS TotalCount, SUM(sample_count - error_count) AS SuccessfulCount, SUM(error_count) AS FailedCount FROM cm_test_transactions WHERE test_id = :test_id";

	private final String HTTP_RESPONSES_DISTRIBUTION = "SELECT response_code, SUM(status_count) as statusCount FROM cm_test_transaction_responses WHERE test_id = :test_id GROUP BY response_code ORDER BY response_code";

	/*4*/
	private final String SLA_DISTRIBUTION_ALL = "SELECT COUNT(CASE WHEN (response_time/1000) <= 5 THEN 1 END) AS a, COUNT(CASE WHEN (response_time/1000) > 5 THEN 1 END) AS b FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true'";
	
	private final String RESPONSE_TIME_DISTRIBUTION_ALL = "SELECT COUNT(CASE WHEN response_time/1000 BETWEEN 0 AND 1 THEN 1 END) AS a, COUNT(CASE WHEN response_time/1000 BETWEEN 1 AND 5 THEN 1 END) AS b,  COUNT(CASE WHEN response_time/1000 BETWEEN 5 AND 1000 THEN 1 END) AS c FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true'";

	private final String RESPONSE_TIME_DISTRIBUTION_SINGLE = "SELECT COUNT(CASE WHEN response_time/1000 BETWEEN 0 AND 1 THEN 1 END) AS a, COUNT(CASE WHEN response_time/1000 BETWEEN 1 AND 5 THEN 1 END) AS b,  COUNT(CASE WHEN response_time/1000 BETWEEN 5 AND 1000 THEN 1 END) AS c FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' AND transaction_code = :transaction_code";

	/*4*/
	
	private final String HTTP_RESPONSES_DISTRIBUTION_SINGLE = "SELECT response_code, count FROM cm_test_transaction_responses WHERE test_id = :test_id AND transaction_code = :transaction_code ORDER BY response_code";

	private final String SLA_DISTRIBUTION_SINGLE = "SELECT COUNT(CASE WHEN (response_time/1000) <= ((SELECT sla_value FROM cm_test_transactions WHERE transaction_code=:transaction_code)/1000) THEN 1 END) AS a, COUNT(CASE WHEN (response_time/1000) > ((SELECT sla_value FROM cm_test_transactions WHERE transaction_code=:transaction_code)/1000) THEN 1 END) AS b FROM cm_test_results WHERE test_id = :test_id AND success_status = 'true' AND transaction_code =:transaction_code";

	private final String TRANSACTION_STATUS_DISTRIBUTION_SINGLE = "SELECT sample_count AS TotalCount, (sample_count - error_count) AS SuccessfulCount, error_count AS FailedCount FROM cm_test_transactions WHERE test_id = :test_id AND transaction_code = :transaction_code";

	private final String RESPONSE_TIME_SUMMARY_SINGLE = "SELECT transaction_code, CAST((min_response_time/1000) AS DECIMAL (12,2)) AS minresponsetime, CAST((avg_response_time/1000) AS DECIMAL (12,2)) AS avgresponsetime, CAST((max_response_time/1000) AS DECIMAL (12,2)) AS maxresponsetime, CAST((percentile/1000) AS DECIMAL (12,2)) AS percentile, CAST((std_deviation/1000) AS DECIMAL (12,2)) AS stddeviation, sample_count AS totalcount, (sample_count - error_count) AS successcount, error_count AS failedcount FROM cm_test_transactions  WHERE test_id = :test_id AND transaction_code = :transaction_code";

	private final String GET_TRANSACTION_LIST = "SELECT transaction_code FROM cm_test_transactions WHERE test_id= :test_id";

//	private final String GET_RESPONSE_TIME_CHART_DATA = "SELECT SEC_TO_TIME(ctl.time_index) AS elapsed_time,ctl.all_threads,AVG((response_time/1000)) AS response_time FROM cm_test_load ctl LEFT JOIN (SELECT time_index,response_time FROM cm_test_results WHERE transaction_code = :transaction_code AND test_id = :test_id) ctt  ON (ctt.time_index = ctl.time_index) WHERE test_id= :test_id GROUP BY ctl.time_index ORDER BY  ctl.time_index";

	public static enum ExecutionSummaryQuery {
		LOAD_HITS_PER_SECOND, LOAD_THROUGHPUT, LOAD_ERRORS_PER_SECOND, LOAD_AVERAGE_RESPONSE_TIME, RESPONSE_TIME_SUMMARY
	};

	@Autowired
	ReportRepository(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public String findByTestIdAndTransaction(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("transaction_code", transactionCode);
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(LOAD_RESPONSE_TIME, queryParams, new ResultSetExtractor<String>() {
			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				String s = "";
				if (rs.next()) {
					s = "{\"data\" : [ " + rs.getString("response_values") + "]}";
				}
				return s;
			}
		});
	}

	public String loadUserLoad(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		return jdbcTemplate.query(LOAD_USER_LOAD, queryParams, new ResultSetExtractor<String>() {
			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				String s = "";
				if (rs.next()) {
					s = "{\"data\" : [ " + rs.getString("user_load") + "]}";
				}
				return s;
			}
		});
	}

	public List<ExecSummaryDTO> loadExecutionSummary(String testId, ExecutionSummaryQuery query) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(getExecSummaryQuery(query), queryParams,
				new ResultSetExtractor<List<ExecSummaryDTO>>() {
					@Override
					public List<ExecSummaryDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<ExecSummaryDTO> list = new ArrayList<ExecSummaryDTO>();
						while (rs.next()) {
							ExecSummaryDTO e = new ExecSummaryDTO();
							e.setTime(rs.getInt("time_n_stage"));
							e.setValue(rs.getDouble("value"));
							e.setLoad(rs.getInt("all_threads"));
							list.add(e);
						}
						return list;
					}
				});
	}

	private String getExecSummaryQuery(ExecutionSummaryQuery query) {
		switch (query) {
		case RESPONSE_TIME_SUMMARY:
			return RESPONSE_TIME_SUMMARY;
		case LOAD_HITS_PER_SECOND:
			return LOAD_HITS_PER_SECOND;
		case LOAD_THROUGHPUT:
			return LOAD_THROUGHPUT;
		case LOAD_ERRORS_PER_SECOND:
			return LOAD_ERRORS_PER_SECOND;
		case LOAD_AVERAGE_RESPONSE_TIME:
			return LOAD_AVERAGE_RESPONSE_TIME;
		}
		return null;

	}

	public List<ResponseTimeSummaryDTO> loadResponseTimeSummary(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(RESPONSE_TIME_SUMMARY, queryParams,
				new ResultSetExtractor<List<ResponseTimeSummaryDTO>>() {
					@Override
					public List<ResponseTimeSummaryDTO> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ResponseTimeSummaryDTO> list = new ArrayList<ResponseTimeSummaryDTO>();
						while (rs.next()) {
							ResponseTimeSummaryDTO r = new ResponseTimeSummaryDTO();
							r.setTransactioncode(rs.getString("transaction_code"));
							r.setMinresponsetime(rs.getDouble("minresponsetime"));
							r.setAvgresponsetime(rs.getDouble("avgresponsetime"));
							r.setMaxresponsetime(rs.getDouble("maxresponsetime"));
							r.setPercentile(rs.getDouble("percentile"));
							r.setStddeviation(rs.getDouble("stddeviation"));
							r.setSuccesscount(rs.getInt("successcount"));
							r.setTotalcount(rs.getInt("totalcount"));
							r.setFailedcount(rs.getInt("failedcount"));

							list.add(r);
						}
						return list;
					}
				});
	}

	public List<ResponseTimeSummaryDTO> loadSlowResponseTimes(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(SLOW_RESPONSE_TIME_SUMMARY, queryParams,
				new ResultSetExtractor<List<ResponseTimeSummaryDTO>>() {
					@Override
					public List<ResponseTimeSummaryDTO> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ResponseTimeSummaryDTO> list = new ArrayList<ResponseTimeSummaryDTO>();
						while (rs.next()) {
							ResponseTimeSummaryDTO r = new ResponseTimeSummaryDTO();
							r.setTransactioncode(rs.getString("transaction_code"));
							r.setMinresponsetime(rs.getDouble("minresponsetime"));
							r.setAvgresponsetime(rs.getDouble("avgresponsetime"));
							r.setMaxresponsetime(rs.getDouble("maxresponsetime"));
							r.setPercentile(rs.getDouble("percentile"));
							r.setStddeviation(rs.getDouble("stddeviation"));
							r.setSuccesscount(rs.getInt("successcount"));
							r.setTotalcount(rs.getInt("totalcount"));
							r.setFailedcount(rs.getInt("failedcount"));

							list.add(r);
						}
						return list;
					}
				});
	}

	public DashboardMetricsDTO loadCurrentDashboardMetrics(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		DashboardMetricsDTO dm = new DashboardMetricsDTO();

		dm.setUserload(jdbcTemplate.queryForObject(CURR_USER_LOAD, queryParams, Integer.class));
		dm.setAvgResponseTime(jdbcTemplate.queryForObject(CURR_OVERALL_RESPONSE_TIME, queryParams, Double.class));
		dm.setHitsPerSecond(jdbcTemplate.queryForObject(CURR_HITS_PER_SECOND, queryParams, Double.class));
		dm.setThroughput(jdbcTemplate.queryForObject(CURR_THROUGHPUT_KBPS, queryParams, Double.class));
		dm.setErrorsPerSecond(jdbcTemplate.queryForObject(CURR_ERRORS_PER_SECOND, queryParams, Double.class));

		return dm;
	}

	public TransactionStatusCountDTO loadTransactionStatusCount(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(TRANSACTION_STATUS_DISTRIBUTION, queryParams,
				new ResultSetExtractor<TransactionStatusCountDTO>() {
					@Override
					public TransactionStatusCountDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						TransactionStatusCountDTO ts = new TransactionStatusCountDTO();

						while (rs.next()) {
							ts.setTotalCount(rs.getInt("TotalCount"));
							ts.setSuccessCount(rs.getInt("SuccessfulCount"));
							ts.setFailedCount(rs.getInt("FailedCount"));
						}
						return ts;
					}
				});
	}

	public TransactionStatusCountDTO loadTransactionStatusCountTrans(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		queryParams.put("transaction_code", transactionCode);

		return jdbcTemplate.query(TRANSACTION_STATUS_DISTRIBUTION_SINGLE, queryParams,
				new ResultSetExtractor<TransactionStatusCountDTO>() {
					@Override
					public TransactionStatusCountDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						TransactionStatusCountDTO ts = new TransactionStatusCountDTO();

						while (rs.next()) {
							ts.setTotalCount(rs.getInt("TotalCount"));
							ts.setSuccessCount(rs.getInt("SuccessfulCount"));
							ts.setFailedCount(rs.getInt("FailedCount"));
						}
						return ts;
					}
				});
	}

	public List<HttpResponseCodeDistributionDTO> loadHttpResponseCodesDistribution(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(HTTP_RESPONSES_DISTRIBUTION, queryParams,
				new ResultSetExtractor<List<HttpResponseCodeDistributionDTO>>() {
					@Override
					public List<HttpResponseCodeDistributionDTO> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<HttpResponseCodeDistributionDTO> list = new ArrayList<HttpResponseCodeDistributionDTO>();
						while (rs.next()) {
							HttpResponseCodeDistributionDTO hc = new HttpResponseCodeDistributionDTO();
							hc.setHttpResponseCode(rs.getString("response_code"));
							hc.setCount(rs.getInt("statusCount"));
							list.add(hc);
						}
						return list;
					}
				});
	}

	public List<HttpResponseCodeDistributionDTO> loadHttpResponseCodesDistributionTrans(String testId,
			String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		queryParams.put("transaction_code", transactionCode);

		return jdbcTemplate.query(HTTP_RESPONSES_DISTRIBUTION_SINGLE, queryParams,
				new ResultSetExtractor<List<HttpResponseCodeDistributionDTO>>() {
					@Override
					public List<HttpResponseCodeDistributionDTO> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<HttpResponseCodeDistributionDTO> list = new ArrayList<HttpResponseCodeDistributionDTO>();
						while (rs.next()) {
							HttpResponseCodeDistributionDTO hc = new HttpResponseCodeDistributionDTO();
							hc.setHttpResponseCode(rs.getString("response_code"));
							hc.setCount(rs.getInt("count"));
							list.add(hc);
						}
						return list;
					}
				});
	}

	public ResponseTimeSlaDistributionDTO loadSlaDistributions(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(SLA_DISTRIBUTION_ALL, queryParams,
				new ResultSetExtractor<ResponseTimeSlaDistributionDTO>() {
					@Override
					public ResponseTimeSlaDistributionDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						ResponseTimeSlaDistributionDTO sla = new ResponseTimeSlaDistributionDTO();

						while (rs.next()) {
							sla.setAbove(rs.getInt("a"));
							sla.setBelow(rs.getInt("b"));
						}
						return sla;
					}
				});
	}

	public ResponseTimeSlaDistributionDTO loadSlaDistributionsTrans(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		queryParams.put("transaction_code", transactionCode);

		return jdbcTemplate.query(SLA_DISTRIBUTION_SINGLE, queryParams,
				new ResultSetExtractor<ResponseTimeSlaDistributionDTO>() {
					@Override
					public ResponseTimeSlaDistributionDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						ResponseTimeSlaDistributionDTO sla = new ResponseTimeSlaDistributionDTO();

						while (rs.next()) {
							sla.setAbove(rs.getInt("a"));
							sla.setBelow(rs.getInt("b"));
						}
						return sla;
					}
				});
	}

	public ResponseTimeSummaryDTO loadTransactionSummary(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("transaction_code", transactionCode);
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(RESPONSE_TIME_SUMMARY_SINGLE, queryParams,
				new ResultSetExtractor<ResponseTimeSummaryDTO>() {
					@Override
					public ResponseTimeSummaryDTO extractData(ResultSet rs) throws SQLException, DataAccessException {
						ResponseTimeSummaryDTO rt = new ResponseTimeSummaryDTO();
						while (rs.next()) {
							rt.setTransactioncode(rs.getString("transaction_code"));
							rt.setMinresponsetime(rs.getDouble("minresponsetime"));
							rt.setAvgresponsetime(rs.getDouble("avgresponsetime"));
							rt.setMaxresponsetime(rs.getDouble("maxresponsetime"));
							rt.setPercentile(rs.getDouble("percentile"));
							rt.setStddeviation(rs.getDouble("stddeviation"));
							rt.setSuccesscount(rs.getInt("successcount"));
							rt.setTotalcount(rs.getInt("totalcount"));
							rt.setFailedcount(rs.getInt("failedcount"));
						}
						return rt;
					}
				});
	}

	public TransResponseTimeDistributionDTO loadResponseTimeDistributions(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(RESPONSE_TIME_DISTRIBUTION_ALL, queryParams,
				new ResultSetExtractor<TransResponseTimeDistributionDTO>() {
					@Override
					public TransResponseTimeDistributionDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						TransResponseTimeDistributionDTO trto = new TransResponseTimeDistributionDTO();

						while (rs.next()) {
							trto.setRangeOne(rs.getInt("a"));
							trto.setRangeTwo(rs.getInt("b"));
							trto.setRangeThree(rs.getInt("c"));
						}
						return trto;
					}
				});
	}

	public TransResponseTimeDistributionDTO loadResponseTimeDistributionsTrans(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		queryParams.put("transaction_code", transactionCode);

		return jdbcTemplate.query(RESPONSE_TIME_DISTRIBUTION_SINGLE, queryParams,
				new ResultSetExtractor<TransResponseTimeDistributionDTO>() {
					@Override
					public TransResponseTimeDistributionDTO extractData(ResultSet rs)
							throws SQLException, DataAccessException {

						TransResponseTimeDistributionDTO trto = new TransResponseTimeDistributionDTO();

						while (rs.next()) {
							trto.setRangeOne(rs.getInt("a"));
							trto.setRangeTwo(rs.getInt("b"));
							trto.setRangeThree(rs.getInt("c"));
						}
						return trto;
					}
				});
	}

	public List<String> loadTransactionList(String testId) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);

		return jdbcTemplate.query(GET_TRANSACTION_LIST, queryParams, new ResultSetExtractor<List<String>>() {
			@Override
			public List<String> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> list = new ArrayList<String>();
				while (rs.next()) {
					String tname = rs.getString("transaction_code");
					list.add(tname);
				}
				return list;
			}
		});
	}

	/*public List<ResponseTimeChartDataDTO> loadResponseTimeChartData(String testId, String transactionCode) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("test_id", testId);
		queryParams.put("transaction_code", transactionCode);
		return jdbcTemplate.query(GET_RESPONSE_TIME_CHART_DATA, queryParams,
				new ResultSetExtractor<List<ResponseTimeChartDataDTO>>() {
					@Override
					public List<ResponseTimeChartDataDTO> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<ResponseTimeChartDataDTO> list = new ArrayList<ResponseTimeChartDataDTO>();
						while (rs.next()) {
							ResponseTimeChartDataDTO rtd = new ResponseTimeChartDataDTO();
							rtd.setElapsedTime(rs.getString("elapsed_time"));
							rtd.setAllThreads(rs.getInt("all_threads"));
							rtd.setResponseTime(rs.getDouble("response_time"));
							list.add(rtd);
						}
						return list;
					}
				});
	}*/

}
