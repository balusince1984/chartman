package com.taletellers.chartman.repository;

import com.taletellers.chartman.domain.TestTransaction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TestTransaction entity.
 */
@SuppressWarnings("unused")
public interface TestTransactionRepository extends JpaRepository<TestTransaction,Long> {

	Page<TestTransaction> findByClientTest(Pageable pageable, String code);

	

}
