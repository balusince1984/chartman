package com.taletellers.chartman.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taletellers.chartman.domain.ClientAccount;
import com.taletellers.chartman.repository.ClientAccountRepository;

/**
 * Service Implementation for managing ClientAccount.
 */
@Service
@Transactional
public class ClientAccountService {

    private final Logger log = LoggerFactory.getLogger(ClientAccountService.class);
    
    @Inject
    private ClientAccountRepository clientAccountRepository;

    /**
     * Save a clientAccount.
     *
     * @param clientAccount the entity to save
     * @return the persisted entity
     */
    public ClientAccount save(ClientAccount clientAccount) {
        log.debug("Request to save ClientAccount : {}", clientAccount);
        ClientAccount result = clientAccountRepository.save(clientAccount);
        return result;
    }

    /**
     *  Get all the clientAccounts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<ClientAccount> findAll(Pageable pageable) {
        log.debug("Request to get all ClientAccounts");
        Page<ClientAccount> result = clientAccountRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one clientAccount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public ClientAccount findOne(Long id) {
        log.debug("Request to get ClientAccount : {}", id);
        ClientAccount clientAccount = clientAccountRepository.findOne(id);
        return clientAccount;
    }

    /**
     *  Delete the  clientAccount by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientAccount : {}", id);
        clientAccountRepository.delete(id);
    }
}
