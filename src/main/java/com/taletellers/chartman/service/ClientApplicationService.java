package com.taletellers.chartman.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taletellers.chartman.domain.ClientApplication;
import com.taletellers.chartman.repository.ClientApplicationRepository;

/**
 * Service Implementation for managing ClientApplication.
 */
@Service
@Transactional
public class ClientApplicationService {

	private final Logger log = LoggerFactory.getLogger(ClientApplicationService.class);

	@Inject
	private ClientApplicationRepository clientApplicationRepository;

	/**
	 * Save a clientApplication.
	 *
	 * @param clientApplication
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClientApplication save(ClientApplication clientApplication) {
		log.debug("Request to save ClientApplication : {}", clientApplication);
		if (clientApplication.isActive() == null)
			clientApplication.setActive(false);
		ClientApplication result = clientApplicationRepository.save(clientApplication);
		return result;
	}

	/**
	 * Get all the clientApplications.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClientApplication> findAll(Pageable pageable) {
		log.debug("Request to get all ClientApplications");
		Page<ClientApplication> result = clientApplicationRepository.findAll(pageable);
		return result;
	}

	/**
	 * Get all the clientApplications.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClientApplication> findByAccountId(Long accountId, Pageable pageable) {
		log.debug("Request to get all ClientApplications");
		Page<ClientApplication> result = clientApplicationRepository.findByClientAccountId(accountId, pageable);
		return result;
	}

	/**
	 * Get one clientApplication by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClientApplication findOne(Long id) {
		log.debug("Request to get ClientApplication : {}", id);
		ClientApplication clientApplication = clientApplicationRepository.findOne(id);
		return clientApplication;
	}

	/**
	 * Delete the clientApplication by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ClientApplication : {}", id);
		clientApplicationRepository.delete(id);
	}
}
