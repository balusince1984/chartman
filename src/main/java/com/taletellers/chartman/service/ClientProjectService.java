package com.taletellers.chartman.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taletellers.chartman.domain.ClientProject;
import com.taletellers.chartman.repository.ClientProjectRepository;

/**
 * Service Implementation for managing ClientProject.
 */
@Service
@Transactional
public class ClientProjectService {

	private final Logger log = LoggerFactory.getLogger(ClientProjectService.class);

	@Inject
	private ClientProjectRepository clientProjectRepository;

	/**
	 * Save a clientProject.
	 *
	 * @param clientProject
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClientProject save(ClientProject clientProject) {
		log.debug("Request to save ClientProject : {}", clientProject);
		if (clientProject.isActive() == null)
			clientProject.setActive(false);
		ClientProject result = clientProjectRepository.save(clientProject);
		return result;
	}

	/**
	 * Get all the clientProjects.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClientProject> findAll(Pageable pageable) {
		log.debug("Request to get all ClientProjects");
		Page<ClientProject> result = clientProjectRepository.findAll(pageable);
		return result;
	}

	/**
	 * Get all the clientProjects.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClientProject> findByClientApplicationId(Long applicationId, Pageable pageable) {
		log.debug("Request to get all ClientProjects");
		Page<ClientProject> result = clientProjectRepository.findByClientApplicationId(applicationId, pageable);
		return result;
	}

	/**
	 * Get one clientProject by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClientProject findOne(Long id) {
		log.debug("Request to get ClientProject : {}", id);
		ClientProject clientProject = clientProjectRepository.findOne(id);
		return clientProject;
	}

	/**
	 * Delete the clientProject by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ClientProject : {}", id);
		clientProjectRepository.delete(id);
	}
}
