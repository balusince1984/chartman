package com.taletellers.chartman.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taletellers.chartman.domain.ClientTest;
import com.taletellers.chartman.domain.ClientTestStatus;
import com.taletellers.chartman.repository.ClientTestRepository;

/**
 * Service Implementation for managing ClientTest.
 */
@Service
@Transactional
public class ClientTestService {

	private final Logger log = LoggerFactory.getLogger(ClientTestService.class);

	@Inject
	private ClientTestRepository clientTestRepository;

	/**
	 * Save a clientTest.
	 *
	 * @param clientTest
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClientTest save(ClientTest clientTest) {
		log.debug("Request to save ClientTest : {}", clientTest);
		if (clientTest.isActive() == null)
			clientTest.setActive(false);
		if (clientTest.getClientTestStatus() == null)
			clientTest.setClientTestStatus(ClientTestStatus.RUNNING);

		ClientTest result = clientTestRepository.save(clientTest);
		return result;
	}

	/**
	 * Get all the clientTests.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClientTest> findAll(Pageable pageable) {
		log.debug("Request to get all ClientTests");
		Page<ClientTest> result = clientTestRepository.findAll(pageable);
		return result;
	}

	/**
	 * Get one clientTest by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClientTest findOne(Long id) {
		log.debug("Request to get ClientTest : {}", id);
		ClientTest clientTest = clientTestRepository.findOne(id);
		return clientTest;
	}

	/**
	 * Delete the clientTest by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ClientTest : {}", id);
		clientTestRepository.delete(id);
	}

	public Page<ClientTest> findByClientProjectId(Long projectId, Pageable pageable) {
		log.debug("Request to get all ClientTests by project Id = " + projectId);
		Page<ClientTest> result = clientTestRepository.findByClientProjectId(projectId, pageable);
		return result;
	}
}
