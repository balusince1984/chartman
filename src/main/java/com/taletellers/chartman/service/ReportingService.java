package com.taletellers.chartman.service;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.taletellers.chartman.domain.reporting.DashboardMetricsDTO;
import com.taletellers.chartman.domain.reporting.DashboardMetricsWrapperDTO;
import com.taletellers.chartman.domain.reporting.ExecSummaryDTO;
import com.taletellers.chartman.domain.reporting.HttpResponseCodeDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSlaDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSummaryDTO;
import com.taletellers.chartman.domain.reporting.TransResponseTimeDistributionDTO;
import com.taletellers.chartman.domain.reporting.TransactionDetailsWrapperDTO;
import com.taletellers.chartman.domain.reporting.TransactionStatusCountDTO;
import com.taletellers.chartman.repository.ReportRepository;
import com.taletellers.chartman.repository.ReportRepository.ExecutionSummaryQuery;

@Service
@Transactional
public class ReportingService {

	private final Logger log = LoggerFactory.getLogger(ReportingService.class);

	@Inject
	private ReportRepository reportRepository;

	public String findByTestIdAndTransaction(String testId, String transactionCode) {
		log.debug("Request to get all Transaction Data");
		return reportRepository.findByTestIdAndTransaction(testId, transactionCode);
	}

	public String loadUserLoad(String testId) {
		log.debug("Request to get all ClientProjects");
		String result = reportRepository.loadUserLoad(testId);
		return result;
	}

	public List<ExecSummaryDTO> loadExecutionSummary(String testId, ExecutionSummaryQuery query) {
		log.debug("Request to loadExecutionSummary");
		List<ExecSummaryDTO> result = reportRepository.loadExecutionSummary(testId, query);
		return result;
	}

	public List<ResponseTimeSummaryDTO> loadTransactionSummary(String testId) {
		log.debug("Request to loadExecutionSummary");
		List<ResponseTimeSummaryDTO> result = reportRepository.loadResponseTimeSummary(testId);
		return result;
	}

	/*
	 * public int getCurrentLoad(String testId){ int load =
	 * reportRepository.getCurrentUserLoad(testId); return load; }
	 * 
	 * public double getCurrentHPS(String testId){ double hps =
	 * reportRepository.getCurrentHPS(testId); return hps; }
	 * 
	 * public double getCurrentThrouhput(String testId){ double tpt =
	 * reportRepository.getCurrentThroughput(testId); return tpt; }
	 * 
	 * public double getCurrentEPS(String testId){ double eps =
	 * reportRepository.getCurrentEPS(testId); return eps; }
	 * 
	 * public double getCurrentAvgRespTime(String testId){ double art =
	 * reportRepository.getCurrentAvgResponseTime(testId); return art; }
	 */

	public DashboardMetricsDTO loadDashboardMetrics(String testId) {

		// log.debug("SLA value is " + slaValue);
		DashboardMetricsDTO dm = new DashboardMetricsDTO();

		dm = reportRepository.loadCurrentDashboardMetrics(testId);
		return dm;
	}

	public List<ResponseTimeSummaryDTO> loadSlowResponseTimeSummary(String testId) {

		// log.debug("SLA value is " + slaValue);
		List<ResponseTimeSummaryDTO> rt = reportRepository.loadSlowResponseTimes(testId);
		return rt;
	}

	public ResponseTimeSlaDistributionDTO loadSLADistribution(String testId) {

		// log.debug("SLA value is " + slaValue);
		ResponseTimeSlaDistributionDTO rt = reportRepository.loadSlaDistributions(testId);
		return rt;
	}

	public TransactionStatusCountDTO loadTransactionStatusCount(String testId) {

		TransactionStatusCountDTO rt = reportRepository.loadTransactionStatusCount(testId);
		return rt;
	}

	public TransResponseTimeDistributionDTO loadResponseTimeDistribution(String testId) {
		TransResponseTimeDistributionDTO rt = reportRepository.loadResponseTimeDistributions(testId);
		return rt;
	}

	public List<HttpResponseCodeDistributionDTO> loadResponseCodeDistribution(String testId) {

		// log.debug("SLA value is " + slaValue);
		List<HttpResponseCodeDistributionDTO> rt = reportRepository.loadHttpResponseCodesDistribution(testId);
		return rt;
	}

	public TransactionDetailsWrapperDTO loadTransactionDetails(String testId, String transactionCode) {
		TransactionDetailsWrapperDTO td = new TransactionDetailsWrapperDTO();
		/*
		 * td.setUserLoad(reportRepository.loadUserLoad(testId));
		 * td.setTransactionTrendData(reportRepository.
		 * findByTestIdAndTransaction(testId, transactionCode));
		 */
		td.setTransactionSummary(reportRepository.loadTransactionSummary(testId, transactionCode));
		td.setResponseTimeSlaDistribution(reportRepository.loadSlaDistributionsTrans(testId, transactionCode));
		td.setHttpResponseDistributionList(reportRepository.loadHttpResponseCodesDistributionTrans(testId, transactionCode));
		td.setTransactionStatusCount(reportRepository.loadTransactionStatusCountTrans(testId, transactionCode));
		td.setTransResponseTimeDistribution(reportRepository.loadResponseTimeDistributionsTrans(testId, transactionCode));
		return td;

	}

	public List<String> getTransactionList(String testId) {
		return reportRepository.loadTransactionList(testId);
	}

}
