package com.taletellers.chartman.service;

import com.taletellers.chartman.domain.TestTransaction;
import com.taletellers.chartman.repository.TestTransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing TestTransaction.
 */
@Service
@Transactional
public class TestTransactionService {

    private final Logger log = LoggerFactory.getLogger(TestTransactionService.class);
    
    @Inject
    private TestTransactionRepository testTransactionRepository;

    /**
     * Save a testTransaction.
     *
     * @param testTransaction the entity to save
     * @return the persisted entity
     */
    public TestTransaction save(TestTransaction testTransaction) {
        log.debug("Request to save TestTransaction : {}", testTransaction);
        TestTransaction result = testTransactionRepository.save(testTransaction);
        return result;
    }

    /**
     *  Get all the testTransactions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<TestTransaction> findAll(Pageable pageable) {
        log.debug("Request to get all TestTransactions");
        Page<TestTransaction> result = testTransactionRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one testTransaction by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TestTransaction findOne(Long id) {
        log.debug("Request to get TestTransaction : {}", id);
        TestTransaction testTransaction = testTransactionRepository.findOne(id);
        return testTransaction;
    }

    /**
     *  Delete the  testTransaction by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TestTransaction : {}", id);
        testTransactionRepository.delete(id);
    }
    

    
    @Transactional(readOnly = true) 
    public Page<TestTransaction> findAll(Pageable pageable, String testId) {
        log.debug("Request to get all TestTransactions");
        Page<TestTransaction> result = testTransactionRepository.findByClientTest(pageable, testId);
        log.debug("Request to get all TestTransactions"+result);
        return result;
        
    }
}
