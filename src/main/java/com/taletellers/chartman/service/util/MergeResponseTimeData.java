package com.taletellers.chartman.service.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.taletellers.chartman.domain.reporting.ResponseTimeChartDataDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeDTO;
import com.taletellers.chartman.domain.reporting.UserLoadDTO;
//import com.taletellers.chartman.service.ReportingService;

//@Service
public final class MergeResponseTimeData {
	private final Logger log = LoggerFactory.getLogger(MergeResponseTimeData.class);
	
	private MergeResponseTimeData(){
		
	}
	
   /*public static double toTwoDecimals(double input){
	   input = input * 100;
	   input = (double) Math.round(input);
	   input = input /100;
	   return input;
   }
	
	public static String pad(String str){
		return str.substring((str.length()-2),str.length());
		}
	public static String toHMS(int timeIndexSecs) {
		int minutes = (int) Math.floor(timeIndexSecs / 60);
		int secs = timeIndexSecs % 60;
		int hours = (int) Math.floor(minutes /60);
		minutes = minutes % 60;
		return pad("0"+Integer.toString(hours)) + ":" +pad("0"+Integer.toString(minutes)) + ":" + pad("0"+Integer.toString(secs));
	}
	
	public static List<ResponseTimeChartDataDTO> mergeChartData(List<ResponseTimeDTO> rt, List<UserLoadDTO> ul){
		List<ResponseTimeChartDataDTO> rtcd = new ArrayList<ResponseTimeChartDataDTO>();
		int t = 0;
		//String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		
		//log.debug("before timestamp--->"+timeStamp);
		for (int i=0; i <ul.size(); i++){
			
			ResponseTimeChartDataDTO rtc = new ResponseTimeChartDataDTO();
			
			rtc.setTimeIndex(toHMS(ul.get(i).getTimeIndex()));
			//rtc.setResponseTime(rt.get(t).getResponseTime());
			rtc.setAllThreads(ul.get(i).getAllThreads());
			if((rt.get(t) != null) && ((rt.get(t).getTimeIndex() == ul.get(i).getTimeIndex()))){
				rtc.setTimeIndex(toHMS(ul.get(i).getTimeIndex()));
				rtc.setResponseTime(toTwoDecimals(rt.get(t).getResponseTime()));
				rtc.setAllThreads(ul.get(i).getAllThreads());
			//	log.debug("Response time is --** "+rt.get(t).getResponseTime()+"ti-->"+ul.get(i).getTimeIndex()+"   ul-->"+ul.get(i).getAllThreads()+"rt-->"+rt.get(t).getResponseTime()+"t value is-- "+t+" and I value is   :"+i );
				if(t<rt.size()-1){
					t = t+1;
				}
				
			}else{
				rtc.setTimeIndex(toHMS(ul.get(i).getTimeIndex()));
				rtc.setAllThreads(ul.get(i).getAllThreads());
			}
			rtcd.add(rtc);
		}
		//String timeStamp2 = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		
		//log.debug("After timestamp--->"+timeStamp2);
		return rtcd;
	}*/
}
