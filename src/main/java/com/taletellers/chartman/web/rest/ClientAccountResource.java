package com.taletellers.chartman.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.ClientAccount;
import com.taletellers.chartman.service.ClientAccountService;
import com.taletellers.chartman.web.rest.util.HeaderUtil;
import com.taletellers.chartman.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ClientAccount.
 */
@RestController
@RequestMapping("/api")
public class ClientAccountResource {

    private final Logger log = LoggerFactory.getLogger(ClientAccountResource.class);
        
    @Inject
    private ClientAccountService clientAccountService;

    /**
     * POST  /client-accounts : Create a new clientAccount.
     *
     * @param clientAccount the clientAccount to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientAccount, or with status 400 (Bad Request) if the clientAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-accounts")
    @Timed
    public ResponseEntity<ClientAccount> createClientAccount(@Valid @RequestBody ClientAccount clientAccount) throws URISyntaxException {
        log.debug("REST request to save ClientAccount : {}", clientAccount);
        if (clientAccount.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clientAccount", "idexists", "A new clientAccount cannot already have an ID")).body(null);
        }
        ClientAccount result = clientAccountService.save(clientAccount);
        return ResponseEntity.created(new URI("/api/client-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("clientAccount", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-accounts : Updates an existing clientAccount.
     *
     * @param clientAccount the clientAccount to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientAccount,
     * or with status 400 (Bad Request) if the clientAccount is not valid,
     * or with status 500 (Internal Server Error) if the clientAccount couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-accounts")
    @Timed
    public ResponseEntity<ClientAccount> updateClientAccount(@Valid @RequestBody ClientAccount clientAccount) throws URISyntaxException {
        log.debug("REST request to update ClientAccount : {}", clientAccount);
        if (clientAccount.getId() == null) {
            return createClientAccount(clientAccount);
        }
        ClientAccount result = clientAccountService.save(clientAccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("clientAccount", clientAccount.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-accounts : get all the clientAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientAccounts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/client-accounts")
    @Timed
    public ResponseEntity<List<ClientAccount>> getAllClientAccounts(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ClientAccounts");
        Page<ClientAccount> page = clientAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-accounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-accounts/:id : get the "id" clientAccount.
     *
     * @param id the id of the clientAccount to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientAccount, or with status 404 (Not Found)
     */
    @GetMapping("/client-accounts/{id}")
    @Timed
    public ResponseEntity<ClientAccount> getClientAccount(@PathVariable Long id) {
        log.debug("REST request to get ClientAccount : {}", id);
        ClientAccount clientAccount = clientAccountService.findOne(id);
        return Optional.ofNullable(clientAccount)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /client-accounts/:id : delete the "id" clientAccount.
     *
     * @param id the id of the clientAccount to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientAccount(@PathVariable Long id) {
        log.debug("REST request to delete ClientAccount : {}", id);
        clientAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clientAccount", id.toString())).build();
    }

}
