package com.taletellers.chartman.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.ClientApplication;
import com.taletellers.chartman.service.ClientApplicationService;
import com.taletellers.chartman.web.rest.util.HeaderUtil;
import com.taletellers.chartman.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ClientApplication.
 */
@RestController
@RequestMapping("/api")
public class ClientApplicationResource {

	private final Logger log = LoggerFactory.getLogger(ClientApplicationResource.class);

	@Inject
	private ClientApplicationService clientApplicationService;

	/**
	 * POST /client-applications : Create a new clientApplication.
	 *
	 * @param clientApplication
	 *            the clientApplication to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new clientApplication, or with status 400 (Bad Request) if the
	 *         clientApplication has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/client-applications")
	@Timed
	public ResponseEntity<ClientApplication> createClientApplication(
			@Valid @RequestBody ClientApplication clientApplication) throws URISyntaxException {
		log.debug("REST request to save ClientApplication : {}", clientApplication);
		if (clientApplication.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clientApplication", "idexists",
					"A new clientApplication cannot already have an ID")).body(null);
		}
		ClientApplication result = clientApplicationService.save(clientApplication);
		return ResponseEntity.created(new URI("/api/client-applications/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("clientApplication", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /client-applications : Updates an existing clientApplication.
	 *
	 * @param clientApplication
	 *            the clientApplication to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clientApplication, or with status 400 (Bad Request) if the
	 *         clientApplication is not valid, or with status 500 (Internal
	 *         Server Error) if the clientApplication couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/client-applications")
	@Timed
	public ResponseEntity<ClientApplication> updateClientApplication(
			@Valid @RequestBody ClientApplication clientApplication) throws URISyntaxException {
		log.debug("REST request to update ClientApplication : {}", clientApplication);
		if (clientApplication.getId() == null) {
			return createClientApplication(clientApplication);
		}
		ClientApplication result = clientApplicationService.save(clientApplication);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clientApplication", clientApplication.getId().toString()))
				.body(result);
	}

	/**
	 * GET /client-applications : get all the clientApplications.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientApplications in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/client-applications")
	@Timed
	public ResponseEntity<List<ClientApplication>> getAllClientApplications(@ApiParam Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of ClientApplications");
		Page<ClientApplication> page = clientApplicationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-applications");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-applications : get all the clientApplications.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientApplications in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(path = "/client-applications", params = { "accountId" })
	@Timed
	public ResponseEntity<List<ClientApplication>> getAllClientApplicationsByAccountId(@ApiParam Pageable pageable,
			@RequestParam(value = "accountId") Long accountId) throws URISyntaxException {
		log.debug("REST request to get a page of ClientApplications");
		Page<ClientApplication> page = clientApplicationService.findByAccountId(accountId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-applications");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-applications/:id : get the "id" clientApplication.
	 *
	 * @param id
	 *            the id of the clientApplication to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clientApplication, or with status 404 (Not Found)
	 */
	@GetMapping("/client-applications/{id}")
	@Timed
	public ResponseEntity<ClientApplication> getClientApplication(@PathVariable Long id) {
		log.debug("REST request to get ClientApplication : {}", id);
		ClientApplication clientApplication = clientApplicationService.findOne(id);
		return Optional.ofNullable(clientApplication).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /client-applications/:id : delete the "id" clientApplication.
	 *
	 * @param id
	 *            the id of the clientApplication to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/client-applications/{id}")
	@Timed
	public ResponseEntity<Void> deleteClientApplication(@PathVariable Long id) {
		log.debug("REST request to delete ClientApplication : {}", id);
		clientApplicationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clientApplication", id.toString()))
				.build();
	}

}
