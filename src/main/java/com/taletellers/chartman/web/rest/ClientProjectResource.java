package com.taletellers.chartman.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.ClientProject;
import com.taletellers.chartman.service.ClientProjectService;
import com.taletellers.chartman.web.rest.util.HeaderUtil;
import com.taletellers.chartman.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ClientProject.
 */
@RestController
@RequestMapping("/api")
public class ClientProjectResource {

	private final Logger log = LoggerFactory.getLogger(ClientProjectResource.class);

	@Inject
	private ClientProjectService clientProjectService;

	/**
	 * POST /client-projects : Create a new clientProject.
	 *
	 * @param clientProject
	 *            the clientProject to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new clientProject, or with status 400 (Bad Request) if the
	 *         clientProject has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/client-projects")
	@Timed
	public ResponseEntity<ClientProject> createClientProject(@Valid @RequestBody ClientProject clientProject)
			throws URISyntaxException {
		log.debug("REST request to save ClientProject : {}", clientProject);
		if (clientProject.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clientProject", "idexists",
					"A new clientProject cannot already have an ID")).body(null);
		}
		ClientProject result = clientProjectService.save(clientProject);
		return ResponseEntity.created(new URI("/api/client-projects/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("clientProject", result.getId().toString())).body(result);
	}

	/**
	 * PUT /client-projects : Updates an existing clientProject.
	 *
	 * @param clientProject
	 *            the clientProject to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clientProject, or with status 400 (Bad Request) if the
	 *         clientProject is not valid, or with status 500 (Internal Server
	 *         Error) if the clientProject couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/client-projects")
	@Timed
	public ResponseEntity<ClientProject> updateClientProject(@Valid @RequestBody ClientProject clientProject)
			throws URISyntaxException {
		log.debug("REST request to update ClientProject : {}", clientProject);
		if (clientProject.getId() == null) {
			return createClientProject(clientProject);
		}
		ClientProject result = clientProjectService.save(clientProject);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clientProject", clientProject.getId().toString()))
				.body(result);
	}

	/**
	 * GET /client-projects : get all the clientProjects.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientProjects in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/client-projects")
	@Timed
	public ResponseEntity<List<ClientProject>> getAllClientProjects(@ApiParam Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of ClientProjects");
		Page<ClientProject> page = clientProjectService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-projects");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-projects : get all the clientProjects.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientProjects in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(path = "/client-projects", params = { "applicationId" })
	@Timed
	public ResponseEntity<List<ClientProject>> getAllClientProjectsByApplication(@ApiParam Pageable pageable,
			@RequestParam(value = "applicationId") Long applicationId) throws URISyntaxException {
		log.debug("REST request to get a page of ClientProjects");
		Page<ClientProject> page = clientProjectService.findByClientApplicationId(applicationId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-projects");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-projects/:id : get the "id" clientProject.
	 *
	 * @param id
	 *            the id of the clientProject to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clientProject, or with status 404 (Not Found)
	 */
	@GetMapping("/client-projects/{id}")
	@Timed
	public ResponseEntity<ClientProject> getClientProject(@PathVariable Long id) {
		log.debug("REST request to get ClientProject : {}", id);
		ClientProject clientProject = clientProjectService.findOne(id);
		return Optional.ofNullable(clientProject).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /client-projects/:id : delete the "id" clientProject.
	 *
	 * @param id
	 *            the id of the clientProject to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/client-projects/{id}")
	@Timed
	public ResponseEntity<Void> deleteClientProject(@PathVariable Long id) {
		log.debug("REST request to delete ClientProject : {}", id);
		clientProjectService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clientProject", id.toString()))
				.build();
	}

}
