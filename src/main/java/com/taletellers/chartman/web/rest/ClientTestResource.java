package com.taletellers.chartman.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.ClientProject;
import com.taletellers.chartman.domain.ClientTest;
import com.taletellers.chartman.service.ClientTestService;
import com.taletellers.chartman.web.rest.util.HeaderUtil;
import com.taletellers.chartman.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing ClientTest.
 */
@RestController
@RequestMapping("/api")
public class ClientTestResource {

	private final Logger log = LoggerFactory.getLogger(ClientTestResource.class);

	@Inject
	private ClientTestService clientTestService;

	/**
	 * POST /client-tests : Create a new clientTest.
	 *
	 * @param clientTest
	 *            the clientTest to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new clientTest, or with status 400 (Bad Request) if the
	 *         clientTest has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/client-tests")
	@Timed
	public ResponseEntity<ClientTest> createClientTest(@Valid @RequestBody ClientTest clientTest)
			throws URISyntaxException {
		log.debug("REST request to save ClientTest : {}", clientTest);
		if (clientTest.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clientTest", "idexists",
					"A new clientTest cannot already have an ID")).body(null);
		}
		clientTest.setTestKey(UUID.randomUUID().toString());
		ClientTest result = clientTestService.save(clientTest);
		return ResponseEntity.created(new URI("/api/client-tests/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("clientTest", result.getId().toString())).body(result);
	}

	/**
	 * PUT /client-tests : Updates an existing clientTest.
	 *
	 * @param clientTest
	 *            the clientTest to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clientTest, or with status 400 (Bad Request) if the clientTest is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         clientTest couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/client-tests")
	@Timed
	public ResponseEntity<ClientTest> updateClientTest(@Valid @RequestBody ClientTest clientTest)
			throws URISyntaxException {
		log.debug("REST request to update ClientTest : {}", clientTest);
		if (clientTest.getId() == null) {
			return createClientTest(clientTest);
		}
		ClientTest result = clientTestService.save(clientTest);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clientTest", clientTest.getId().toString())).body(result);
	}

	/**
	 * GET /client-tests : get all the clientTests.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientTests in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/client-tests")
	@Timed
	public ResponseEntity<List<ClientTest>> getAllClientTests(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of ClientTests");
		Page<ClientTest> page = clientTestService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-tests");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-tests : get all the clienttests.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clientProjects in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(path = "/client-tests", params = { "projectId" })
	@Timed
	public ResponseEntity<List<ClientTest>> getAllClientProjectsByApplication(@ApiParam Pageable pageable,
			@RequestParam(value = "projectId") Long projectId) throws URISyntaxException {
		log.debug("REST request to get a page of ClientProjects");
		Page<ClientTest> page = clientTestService.findByClientProjectId(projectId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-tests");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /client-tests/:id : get the "id" clientTest.
	 *
	 * @param id
	 *            the id of the clientTest to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clientTest, or with status 404 (Not Found)
	 */
	@GetMapping("/client-tests/{id}")
	@Timed
	public ResponseEntity<ClientTest> getClientTest(@PathVariable Long id) {
		log.debug("REST request to get ClientTest : {}", id);
		ClientTest clientTest = clientTestService.findOne(id);
		return Optional.ofNullable(clientTest).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /client-tests/:id : delete the "id" clientTest.
	 *
	 * @param id
	 *            the id of the clientTest to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/client-tests/{id}")
	@Timed
	public ResponseEntity<Void> deleteClientTest(@PathVariable Long id) {
		log.debug("REST request to delete ClientTest : {}", id);
		clientTestService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clientTest", id.toString())).build();
	}

}
