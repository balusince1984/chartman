package com.taletellers.chartman.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.reporting.DashboardMetricsDTO;
import com.taletellers.chartman.domain.reporting.DashboardMetricsWrapperDTO;
import com.taletellers.chartman.domain.reporting.ExecSummaryDTO;
import com.taletellers.chartman.domain.reporting.HttpResponseCodeDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeChartDataDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSlaDistributionDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeSummaryDTO;
import com.taletellers.chartman.domain.reporting.ResponseTimeTrendDataDTO;
import com.taletellers.chartman.domain.reporting.TransResponseTimeDistributionDTO;
import com.taletellers.chartman.domain.reporting.TransactionDetailsWrapperDTO;
import com.taletellers.chartman.domain.reporting.TransactionStatusCountDTO;
import com.taletellers.chartman.repository.ReportRepository.ExecutionSummaryQuery;
import com.taletellers.chartman.service.ReportingService;

/**
 * REST controller for managing TestTransaction.
 */
@RestController
@RequestMapping("/api/reports")
public class ReportingResource {

	private final Logger log = LoggerFactory.getLogger(ReportingResource.class);

	@Inject
	private ReportingService reportingService;

	/**
	 * GET /test-transactions : get all the testTransactions.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         testTransactions in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/responsetimetrend/{testId}/{transactionCode}")
	@Timed
	public ResponseEntity<String> getResponseTimeReport(@PathVariable String testId, @PathVariable String transactionCode) throws URISyntaxException {
		log.debug("REST request to get a page of getResponseTimeReport");
		String result = reportingService.findByTestIdAndTransaction(testId, transactionCode);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * GET /test-transactions : get all the testTransactions.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         testTransactions in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/responsetimetrend/{testId}")
	@Timed
	public ResponseEntity<String> getUserLoadReport(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a page of getResponseTimeReport");
		String result = reportingService.loadUserLoad(testId);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * GET /hitsperseconds : get all the testTransactions.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         testTransactions in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/execsummary/{testId}/{graph}")
	@Timed
	public ResponseEntity<List<ExecSummaryDTO>> getExecutionSummaryReport(@PathVariable String testId, @PathVariable String graph) throws URISyntaxException {
		log.debug("REST request to get a page of getResponseTimeReport");

		List<ExecSummaryDTO> result = reportingService.loadExecutionSummary(testId, ExecutionSummaryQuery.valueOf(graph));
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/execsummary/{testId}/RESPONSETIME_SUMMARY_TABLE")
	@Timed
	public ResponseEntity<List<ResponseTimeSummaryDTO>> getResponseTimeSummaryTable(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a page of getSlowResponseTimeSumamryTable");
		// YOUR CODE HERE
		List<ResponseTimeSummaryDTO> result = reportingService.loadTransactionSummary(testId);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/resultsdashboard/{testId}/DASH_BOARD_METRICS")
	@Timed
	public ResponseEntity<DashboardMetricsDTO> getDashboardMetrics(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a DashboardMetrics");
		// YOUR CODE HERE
		DashboardMetricsDTO dmo = reportingService.loadDashboardMetrics(testId);
		return new ResponseEntity<>(dmo, HttpStatus.OK);
	}

	@GetMapping("/resultsdashboard/{testId}/SLOW_TRANSACTIONS")
	@Timed
	public ResponseEntity<List<ResponseTimeSummaryDTO>> getSlowTransactionsSummary(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a DashboardMetrics");
		// YOUR CODE HERE
		List<ResponseTimeSummaryDTO> rtlist = reportingService.loadSlowResponseTimeSummary(testId);
		return new ResponseEntity<>(rtlist, HttpStatus.OK);
	}
	
	@GetMapping("/resultsdashboard/{testId}/TRANSACTION_RESULTS")
	@Timed
	public ResponseEntity<TransactionStatusCountDTO> getTransactionStatusCount(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a Transaction Status Count");
		// YOUR CODE HERE
		TransactionStatusCountDTO ts = reportingService.loadTransactionStatusCount(testId);
		return new ResponseEntity<>(ts, HttpStatus.OK);
	}

	@GetMapping("/resultsdashboard/{testId}/SLA_RESULTS")
	@Timed
	public ResponseEntity<ResponseTimeSlaDistributionDTO> getSLADistribution(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a Transaction Status Count");
		// YOUR CODE HERE
		ResponseTimeSlaDistributionDTO rs = reportingService.loadSLADistribution(testId);
		return new ResponseEntity<>(rs, HttpStatus.OK);
	}
	
	@GetMapping("/resultsdashboard/{testId}/RESPONSE_TIME_DISTRIBUTION")
	@Timed
	public ResponseEntity<TransResponseTimeDistributionDTO> getResponseTimeDistribution(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a Transaction Status Count");
		// YOUR CODE HERE
		TransResponseTimeDistributionDTO rs = reportingService.loadResponseTimeDistribution(testId);
		return new ResponseEntity<>(rs, HttpStatus.OK);
	}
	
	@GetMapping("/resultsdashboard/{testId}/HTTP_RESPONSE_CODES")
	@Timed
	public ResponseEntity<List<HttpResponseCodeDistributionDTO>> getResponseCodeDistribution(@PathVariable String testId) throws URISyntaxException {
		log.debug("REST request to get a Transaction Status Count");
		// YOUR CODE HERE
		List<HttpResponseCodeDistributionDTO> rs = reportingService.loadResponseCodeDistribution(testId);
		return new ResponseEntity<>(rs, HttpStatus.OK);
	}
	
	@GetMapping("/transactiondetails/{testId}/{transactionCode}")
	@Timed
	public ResponseEntity<TransactionDetailsWrapperDTO> getTransactionDetails(@PathVariable String testId, @PathVariable String transactionCode) throws URISyntaxException {
		log.debug("REST request to get a DashboardMetrics");
		// YOUR CODE HERE
		TransactionDetailsWrapperDTO tdo = reportingService.loadTransactionDetails(testId, transactionCode);
		return new ResponseEntity<>(tdo, HttpStatus.OK);
	}

	@GetMapping("/transactionlist/{testId}")
	@Timed
	public ResponseEntity<List<String>> getTransactionList(@PathVariable String testId) {
		List<String> tList = new ArrayList<String>();
		tList = reportingService.getTransactionList(testId);
		return new ResponseEntity<>(tList, HttpStatus.OK);
	}

}
