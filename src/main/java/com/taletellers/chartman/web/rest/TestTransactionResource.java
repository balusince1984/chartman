package com.taletellers.chartman.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.taletellers.chartman.domain.TestTransaction;
import com.taletellers.chartman.service.TestTransactionService;
import com.taletellers.chartman.web.rest.util.HeaderUtil;
import com.taletellers.chartman.web.rest.util.PaginationUtil;

import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TestTransaction.
 */
@RestController
@RequestMapping("/api")
public class TestTransactionResource {

    private final Logger log = LoggerFactory.getLogger(TestTransactionResource.class);
        
    @Inject
    private TestTransactionService testTransactionService;

    /**
     * POST  /test-transactions : Create a new testTransaction.
     *
     * @param testTransaction the testTransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new testTransaction, or with status 400 (Bad Request) if the testTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/test-transactions")
    @Timed
    public ResponseEntity<TestTransaction> createTestTransaction(@RequestBody TestTransaction testTransaction) throws URISyntaxException {
        log.debug("REST request to save TestTransaction : {}", testTransaction);
        TestTransaction result = testTransactionService.save(testTransaction);
        return ResponseEntity.created(new URI("/api/test-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("testTransaction", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /test-transactions : Updates an existing testTransaction.
     *
     * @param testTransaction the testTransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated testTransaction,
     * or with status 400 (Bad Request) if the testTransaction is not valid,
     * or with status 500 (Internal Server Error) if the testTransaction couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/test-transactions")
    @Timed
    public ResponseEntity<TestTransaction> updateTestTransaction(@RequestBody TestTransaction testTransaction) throws URISyntaxException {
        log.debug("REST request to update TestTransaction : {}", testTransaction);
        TestTransaction result = testTransactionService.save(testTransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("testTransaction", testTransaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /test-transactions : get all the testTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of testTransactions in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/test-transactions")
    @Timed
    public ResponseEntity<List<TestTransaction>> getAllTestTransactions(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TestTransactions");
        Page<TestTransaction> page = testTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/test-transactions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /test-transactions/:id : get the "id" testTransaction.
     *
     * @param id the id of the testTransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the testTransaction, or with status 404 (Not Found)
     */
    @GetMapping("/test-transactions/{id}")
    @Timed
    public ResponseEntity<TestTransaction> getTestTransaction(@PathVariable Long id) {
        log.debug("REST request to get TestTransaction : {}", id);
        TestTransaction testTransaction = testTransactionService.findOne(id);
        return Optional.ofNullable(testTransaction)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /test-transactions/:id : delete the "id" testTransaction.
     *
     * @param id the id of the testTransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/test-transactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteTestTransaction(@PathVariable Long id) {
        log.debug("REST request to delete TestTransaction : {}", id);
        testTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("testTransaction", id.toString())).build();
    }

    @GetMapping(path = "/test-transactions", params = { "testId"})
    @Timed
    public ResponseEntity<List<TestTransaction>> getAllTransactions(@ApiParam Pageable pageable,
      @RequestParam(value = "testId") Long projectId) throws URISyntaxException {
      log.debug("REST request to get a page of ClientProjects");
      Page<TestTransaction> page = testTransactionService.findAll(pageable);
      HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/test-transactions");
      return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
