/**
 * View Models used by Spring MVC REST controllers.
 */
package com.taletellers.chartman.web.rest.vm;
