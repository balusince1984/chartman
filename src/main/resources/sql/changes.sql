/* :001 Added by RRP Feb 2 2017*/
ALTER table cm_test_results
CHANGE testId test_id int(11),
CHANGE testName test_name varchar(100),
CHANGE eTimeStamp time_stamp bigint(20),
CHANGE responseTime response_time int(11),
CHANGE transactionName transaction_code varchar(150),
CHANGE responseCode response_code varchar(100),
CHANGE threadName thread_name varchar(100),
CHANGE success success_status varchar(100),
CHANGE grpThreads grp_threads int(8),
CHANGE allThreads all_threads int(8),
CHANGE Latency latency int(10),
CHANGE SampleCount sample_count int(10),
CHANGE ErrorCount error_count int(10),
CHANGE AgentName agent_name varchar(100);
/* :001*/

/* :002 Added by RRP Feb 2 2017 */
ALTER TABLE cm_test_transactions
ADD min_response_time int(11),
ADD avg_response_time int(11), 
ADD max_response_time int(11),
ADD std_deviation int(11),
ADD percentile int(11),
ADD sample_count int(10),
ADD error_count int(10);
/* :002*/

/* :003 Added by RRP Feb 2 2017 */
alter table cm_test_results drop column test_name;
/* :003 */

/* :004 Added by Bala Feb 2 2017 */
ALTER TABLE `cm_test_results` CHANGE `transaction_code` `transaction_code` VARCHAR(150) CHARSET utf8 COLLATE utf8_bin NULL AFTER `test_id`; 
/* :004 Added by Bala Feb 2 2017 */


/* 005 - Added by RRP on Feb 16 2017 */
CREATE TABLE `cm_test_transaction_responses`( `test_id` BIGINT(25) NOT NULL, `transaction_code` VARCHAR(150) NOT NULL, `response_code` VARCHAR(100) NOT NULL, `count` INT(10), PRIMARY KEY (`test_id`, `transaction_code`, `response_code`) ) ENGINE=INNODB; 
/* 005 */

/* 006 - Added by Bala on Feb 16 2017 */
ALTER TABLE `cm_test_transaction_responses` ADD FOREIGN KEY (`test_id`) REFERENCES `cm_client_test`(`test_id`) ON UPDATE RESTRICT ON DELETE RESTRICT; 
/* 006 */


/* 007 Added new column to SLA table */
ALTER TABLE `cm_test_transactions`   
  ADD COLUMN `sla_value` INT(11) NULL AFTER `error_count`;
/* 007*/
  
/* 008 Added by Bala March 01 2017 */
  ALTER TABLE `cm_test_results` CHANGE `test_id` `test_id` BIGINT(20) NULL, ADD FOREIGN KEY (`test_id`) REFERENCES `cm_client_test`(`test_id`) ON UPDATE RESTRICT ON DELETE RESTRICT; 
  CREATE TABLE `cm_test_load` (
  `test_id` bigint(20) NOT NULL,
  `time_index` int(10) NOT NULL,
  `all_threads` int(4) DEFAULT NULL,
  PRIMARY KEY (`test_id`,`time_index`),
  CONSTRAINT `cm_test_load_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `cm_client_test` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/* 008*/

/* 009 Added by Bala March 13 2017 */
ALTER TABLE `cm_test_results` ADD COLUMN `index` BIGINT(20) NULL AUTO_INCREMENT FIRST, CHANGE `time_index` `time_index` INT(11) NULL, ADD KEY(`index`); 

ALTER TABLE `cm_test_results` ADD PRIMARY KEY (`index`); 

ALTER TABLE `cm_test_transactions` ADD COLUMN `response_values` TEXT NULL AFTER `sla_value`; 

ALTER TABLE `cm_client_test` ADD COLUMN `user_load` LONGTEXT NULL AFTER `start_timestamp`; 
/* 009 */

/* 010 Added by Bala March 15 2017 */
ALTER TABLE `cm_client_test` ADD COLUMN `current_time_index` INT(10) NULL AFTER `user_load`, ADD COLUMN `current_user_load` INT(10) NULL AFTER `current_time_index`; 

ALTER TABLE `cm_test_transactions` ADD COLUMN `satisfy_value` INT(10) DEFAULT 1000 NULL AFTER `response_values`, ADD COLUMN `tolerate_value` INT(10) DEFAULT 3000 NULL AFTER `satisfy_value`; 

ALTER TABLE `cm_test_transactions` DROP COLUMN `transaction_id`, DROP PRIMARY KEY; 

ALTER TABLE `cm_test_transactions` CHANGE `test_id` `test_id` BIGINT(20) NOT NULL, CHANGE `transaction_code` `transaction_code` VARCHAR(255) CHARSET utf8 COLLATE utf8_bin NOT NULL, ADD PRIMARY KEY (`test_id`, `transaction_code`); 

ALTER TABLE `cm_test_transactions` ADD FOREIGN KEY (`test_id`) REFERENCES `cm_client_test`(`test_id`) ON UPDATE RESTRICT ON DELETE RESTRICT; 

ALTER TABLE `cm_test_results` ADD FOREIGN KEY (`transaction_code`) REFERENCES `cm_test_transactions`(`transaction_code`); 

ALTER TABLE `cm_test_transactions` DROP FOREIGN KEY `cm_test_transactions_ibfk_1`; 

ALTER TABLE `cm_test_transactions` ADD FOREIGN KEY (`test_id`, `transaction_code`) REFERENCES `cm_test_transactions`(`test_id`, `transaction_code`); 

ALTER TABLE `cm_test_results` DROP FOREIGN KEY `cm_test_results_ibfk_1`; 

ALTER TABLE `cm_test_results` ADD FOREIGN KEY (`test_id`, `transaction_code`) REFERENCES `cm_test_transactions`(`test_id`, `transaction_code`); 

ALTER TABLE `cm_test_transaction_responses` DROP FOREIGN KEY `cm_test_transaction_responses_ibfk_1`; 

ALTER TABLE `cm_test_transaction_responses` CHANGE `transaction_code` `transaction_code` VARCHAR(255) CHARSET utf8 COLLATE utf8_general_ci NOT NULL;
/* 010 */

/*011  Added by RRP on 16 March 2017*/
ALTER TABLE `chartman`.`cm_test_transaction_responses` CHANGE `count` `status_count` INT(10) NULL;
/*011*/