DELIMITER $$

USE `chartman`$$

DROP PROCEDURE IF EXISTS `sp_transformdata`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_transformdata`(IN in_test_id VARCHAR(100),IN in_time_stamp BIGINT(20),IN in_response_time INT(5),IN in_transaction_code VARCHAR(255),IN in_response_code VARCHAR(255),IN in_thread_name VARCHAR(255),IN in_success_status VARCHAR(10),IN in_bytes INT(10),IN in_grp_threads INT(5),IN in_all_threads INT(10),IN in_latency INT(10),IN in_sample_count INT(10),IN in_error_count INT(5),IN in_agent_name VARCHAR(255),IN in_first_timestamp BIGINT(20))
BEGIN
        DECLARE v_time_index INT(10);
        DECLARE v_response_json VARCHAR(100);
        DECLARE v_ul_json VARCHAR(100);
        SET v_time_index = (in_time_stamp - in_first_timestamp) / 1000;
        SET v_response_json = CONCAT('{"ti"',':',v_time_index,',','"rt"',':',in_response_time,',','"ul"',':',in_all_threads,'}');
        SET v_ul_json = CONCAT('{"ti"',':',v_time_index,',','"ul"',':',in_all_threads,'}');        
        UPDATE cm_client_test SET user_load = IF(user_load IS NULL,v_ul_json,CONCAT(user_load,',',v_ul_json)) WHERE test_id = in_test_id;                
        INSERT INTO cm_test_results(test_id,time_stamp,response_time,transaction_code,response_code,thread_name,success_status,bytes,grp_threads,all_threads,latency,sample_count,error_count,agent_name,time_index,time_n_stage) VALUES(in_test_id,in_time_stamp,in_response_time,in_transaction_code,in_response_code,in_thread_name,in_success_status,in_bytes,in_grp_threads,in_all_threads,in_latency,in_sample_count,in_error_count,in_agent_name,v_time_index,FLOOR(v_time_index / 10));
        IF in_success_status = "true" THEN
		 INSERT INTO cm_test_transactions(test_id,transaction_code,min_response_time,max_response_time, avg_response_time,percentile,std_deviation,sample_count,error_count,response_values) VALUES(in_test_id,in_transaction_code,in_response_time,in_response_time,in_response_time,in_response_time,in_response_time,in_sample_count,in_error_count,v_response_json) ON DUPLICATE KEY UPDATE min_response_time=LEAST(COALESCE(min_response_time,99999999999),in_response_time), max_response_time=GREATEST(COALESCE(max_response_time,0),in_response_time), avg_response_time=((COALESCE(avg_response_time,in_response_time)+in_response_time)/2), percentile = (SELECT response_time FROM (SELECT * FROM (SELECT  response_time, (@cum_cnt:=@cum_cnt +  1) / @cnt AS p1 FROM cm_test_results JOIN (SELECT @cum_cnt:=0, @cnt:=COUNT(*) FROM cm_test_results WHERE transaction_code=in_transaction_code AND test_id=in_test_id AND success_status="true") p WHERE transaction_code=in_transaction_code AND test_id=in_test_id AND success_status="true" ORDER BY response_time) t1 HAVING p1 >= .95 LIMIT 1) temp), std_deviation = (SELECT STDDEV(response_time) FROM cm_test_results WHERE test_id = in_test_id AND transaction_code = in_transaction_code ),sample_count=sample_count+in_sample_count,error_count=error_count+in_error_count,response_values = CONCAT(response_values,',',v_response_json);
	ELSE
	        INSERT INTO cm_test_transactions(test_id,transaction_code,min_response_time,max_response_time, avg_response_time,percentile,std_deviation,sample_count,error_count) VALUES(in_test_id,in_transaction_code,NULL,NULL,NULL,NULL,NULL,in_sample_count,in_error_count) ON DUPLICATE KEY UPDATE sample_count=sample_count+in_sample_count,error_count=error_count+in_error_count;
        END IF;
        INSERT INTO `cm_test_transaction_responses`(`test_id`,`transaction_code`,`response_code`,`count`) VALUES(in_test_id,in_transaction_code,in_response_code,1) ON DUPLICATE KEY UPDATE `count` = `count` + 1;         
 END$$

DELIMITER ;