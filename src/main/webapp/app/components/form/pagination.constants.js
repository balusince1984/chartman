(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
