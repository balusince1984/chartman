(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientAccountDetailController', ClientAccountDetailController);

    ClientAccountDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ClientAccount', 'Organisation'];

    function ClientAccountDetailController($scope, $rootScope, $stateParams, previousState, entity, ClientAccount, Organisation) {
        var vm = this;

        vm.clientAccount = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('chartmanApp:clientAccountUpdate', function(event, result) {
            vm.clientAccount = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
