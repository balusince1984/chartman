(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('client-account', {
            parent: 'entity',
            url: '/client-account?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientAccount.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-account/client-accounts.html',
                    controller: 'ClientAccountController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientAccount');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('client-account-detail', {
            parent: 'entity',
            url: '/client-account/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientAccount.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-account/client-account-detail.html',
                    controller: 'ClientAccountDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientAccount');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClientAccount', function($stateParams, ClientAccount) {
                    return ClientAccount.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'client-account',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('client-account-detail.edit', {
            parent: 'client-account-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-account/client-account-dialog.html',
                    controller: 'ClientAccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientAccount', function(ClientAccount) {
                            return ClientAccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-account.new', {
            parent: 'client-account',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-account/client-account-dialog.html',
                    controller: 'ClientAccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                accountName: null,
                                accountDescription: null,
                                accountStatus: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('client-account', null, { reload: 'client-account' });
                }, function() {
                    $state.go('client-account');
                });
            }]
        })
        .state('client-account.edit', {
            parent: 'client-account',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-account/client-account-dialog.html',
                    controller: 'ClientAccountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientAccount', function(ClientAccount) {
                            return ClientAccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-account', null, { reload: 'client-account' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-account.delete', {
            parent: 'client-account',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-account/client-account-delete-dialog.html',
                    controller: 'ClientAccountDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClientAccount', function(ClientAccount) {
                            return ClientAccount.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-account', null, { reload: 'client-account' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
