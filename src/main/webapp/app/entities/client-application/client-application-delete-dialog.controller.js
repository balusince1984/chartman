(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientApplicationDeleteController',ClientApplicationDeleteController);

    ClientApplicationDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClientApplication'];

    function ClientApplicationDeleteController($uibModalInstance, entity, ClientApplication) {
        var vm = this;

        vm.clientApplication = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClientApplication.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
