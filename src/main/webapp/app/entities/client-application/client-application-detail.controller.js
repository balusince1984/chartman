(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientApplicationDetailController', ClientApplicationDetailController);

    ClientApplicationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ClientApplication', 'ClientAccount'];

    function ClientApplicationDetailController($scope, $rootScope, $stateParams, previousState, entity, ClientApplication, ClientAccount) {
        var vm = this;

        vm.clientApplication = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('chartmanApp:clientApplicationUpdate', function(event, result) {
            vm.clientApplication = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
