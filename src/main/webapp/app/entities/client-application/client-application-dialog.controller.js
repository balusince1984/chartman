(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientApplicationDialogController', ClientApplicationDialogController);

    ClientApplicationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'ClientApplication', 'ClientAccount'];

    function ClientApplicationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, ClientApplication, ClientAccount) {
        var vm = this;

        vm.clientApplication = entity;
        vm.clear = clear;
        vm.save = save;
        vm.clientaccounts = ClientAccount.query({filter: 'account_id-is-null'});
        /*$q.all([vm.clientApplication.$promise, vm.clientaccounts.$promise]).then(function() {
        	console.log(vm.clientApplication.clientAccount);
            if (!vm.clientApplication.clientAccount || !vm.clientApplication.clientAccount.id) {
                return $q.reject();
            }
            return ClientAccount.get({id : vm.clientApplication.clientAccount.id}).$promise;
        }).then(function(clientAccount) {
            vm.clientaccounts.push(clientAccount);
        });*/

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.clientApplication.id !== null) {
                ClientApplication.update(vm.clientApplication, onSaveSuccess, onSaveError);
            } else {
                ClientApplication.save(vm.clientApplication, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('chartmanApp:clientApplicationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
