(function() {
    'use strict';
    angular
        .module('chartmanApp')
        .factory('ClientApplication', ClientApplication);

    ClientApplication.$inject = ['$resource'];

    function ClientApplication ($resource) {
        var resourceUrl =  'api/client-applications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
