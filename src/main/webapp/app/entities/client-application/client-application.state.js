(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('client-application', {
            parent: 'entity',
            url: '/client-application?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientApplication.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-application/client-applications.html',
                    controller: 'ClientApplicationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientApplication');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('client-application-detail', {
            parent: 'entity',
            url: '/client-application/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientApplication.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-application/client-application-detail.html',
                    controller: 'ClientApplicationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientApplication');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClientApplication', function($stateParams, ClientApplication) {
                    return ClientApplication.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'client-application',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('client-application-detail.edit', {
            parent: 'client-application-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-application/client-application-dialog.html',
                    controller: 'ClientApplicationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientApplication', function(ClientApplication) {
                            return ClientApplication.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-application.new', {
            parent: 'client-application',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-application/client-application-dialog.html',
                    controller: 'ClientApplicationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                applicationName: null,
                                applicationDescription: null,
                                active: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('client-application', null, { reload: 'client-application' });
                }, function() {
                    $state.go('client-application');
                });
            }]
        })
        .state('client-application.edit', {
            parent: 'client-application',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-application/client-application-dialog.html',
                    controller: 'ClientApplicationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientApplication', function(ClientApplication) {
                            return ClientApplication.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-application', null, { reload: 'client-application' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-application.delete', {
            parent: 'client-application',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-application/client-application-delete-dialog.html',
                    controller: 'ClientApplicationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClientApplication', function(ClientApplication) {
                            return ClientApplication.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-application', null, { reload: 'client-application' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
