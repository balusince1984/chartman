(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientProjectDeleteController',ClientProjectDeleteController);

    ClientProjectDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClientProject'];

    function ClientProjectDeleteController($uibModalInstance, entity, ClientProject) {
        var vm = this;

        vm.clientProject = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClientProject.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
