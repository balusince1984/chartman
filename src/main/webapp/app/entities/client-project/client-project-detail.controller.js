(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientProjectDetailController', ClientProjectDetailController);

    ClientProjectDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ClientProject'];

    function ClientProjectDetailController($scope, $rootScope, $stateParams, previousState, entity, ClientProject) {
        var vm = this;

        vm.clientProject = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('chartmanApp:clientProjectUpdate', function(event, result) {
            vm.clientProject = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
