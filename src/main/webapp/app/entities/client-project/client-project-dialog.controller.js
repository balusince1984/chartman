(function() {
	'use strict';

	angular.module('chartmanApp').controller('ClientProjectDialogController', ClientProjectDialogController);

	ClientProjectDialogController.$inject = [ '$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ClientProject', 'ClientAccount', 'ClientApplication' ];

	function ClientProjectDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ClientProject, ClientAccount, ClientApplication) {
		var vm = this;

		vm.clientProject = entity;
		if (vm.clientProject.clientApplication)
			vm.clientAccount = vm.clientProject.clientApplication.clientAccount
		vm.clear = clear;
		vm.save = save;
		vm.loadClientApplications = loadClientApplications;

		if (vm.clientProject.id > 0) {
			loadClientApplications();
		}

		vm.clientaccounts = ClientAccount.query({
			filter : 'account_id-is-null'
		});

		$timeout(function() {
			angular.element('.form-group:eq(1)>input').focus();
		});

		function clear() {
			$uibModalInstance.dismiss('cancel');
		}

		function loadClientApplications() {
			vm.clientApplications = [];
			if (vm.clientAccount && vm.clientAccount.id) {
				vm.clientApplications = ClientApplication.query({
					accountId : vm.clientAccount.id
				});
			}
		}

		function save() {
			vm.isSaving = true;
			if (vm.clientProject.id !== null) {
				ClientProject.update(vm.clientProject, onSaveSuccess, onSaveError);
			} else {
				ClientProject.save(vm.clientProject, onSaveSuccess, onSaveError);
			}
		}

		function onSaveSuccess(result) {
			$scope.$emit('chartmanApp:clientProjectUpdate', result);
			$uibModalInstance.close(result);
			vm.isSaving = false;
		}

		function onSaveError() {
			vm.isSaving = false;
		}

	}
})();
