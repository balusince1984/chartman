(function() {
    'use strict';
    angular
        .module('chartmanApp')
        .factory('ClientProject', ClientProject);

    ClientProject.$inject = ['$resource'];

    function ClientProject ($resource) {
        var resourceUrl =  'api/client-projects/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
