(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('client-project', {
            parent: 'entity',
            url: '/client-project?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientProject.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-project/client-projects.html',
                    controller: 'ClientProjectController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientProject');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('client-project-detail', {
            parent: 'entity',
            url: '/client-project/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientProject.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-project/client-project-detail.html',
                    controller: 'ClientProjectDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientProject');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClientProject', function($stateParams, ClientProject) {
                    return ClientProject.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'client-project',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('client-project-detail.edit', {
            parent: 'client-project-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-project/client-project-dialog.html',
                    controller: 'ClientProjectDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientProject', function(ClientProject) {
                            return ClientProject.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-project.new', {
            parent: 'client-project',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-project/client-project-dialog.html',
                    controller: 'ClientProjectDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                projectName: null,
                                projectDescription: null,
                                active: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('client-project', null, { reload: 'client-project' });
                }, function() {
                    $state.go('client-project');
                });
            }]
        })
        .state('client-project.edit', {
            parent: 'client-project',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-project/client-project-dialog.html',
                    controller: 'ClientProjectDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientProject', function(ClientProject) {
                            return ClientProject.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-project', null, { reload: 'client-project' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-project.delete', {
            parent: 'client-project',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-project/client-project-delete-dialog.html',
                    controller: 'ClientProjectDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClientProject', function(ClientProject) {
                            return ClientProject.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-project', null, { reload: 'client-project' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
