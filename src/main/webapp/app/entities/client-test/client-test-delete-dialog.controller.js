(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientTestDeleteController',ClientTestDeleteController);

    ClientTestDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClientTest'];

    function ClientTestDeleteController($uibModalInstance, entity, ClientTest) {
        var vm = this;

        vm.clientTest = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClientTest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
