(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('ClientTestDetailController', ClientTestDetailController);

    ClientTestDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ClientTest'];

    function ClientTestDetailController($scope, $rootScope, $stateParams, previousState, entity, ClientTest) {
        var vm = this;

        vm.clientTest = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('chartmanApp:clientTestUpdate', function(event, result) {
            vm.clientTest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
