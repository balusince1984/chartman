(function() {
	'use strict';

	angular.module('chartmanApp').controller('ClientTestDialogController', ClientTestDialogController);

	ClientTestDialogController.$inject = [ '$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ClientTest', 'ClientAccount', 'ClientApplication', 'ClientProject' ];

	function ClientTestDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ClientTest, ClientAccount, ClientApplication, ClientProject) {
		var vm = this;

		vm.clientTest = entity;
		vm.clear = clear;
		if (vm.clientTest.clientProject) {
			vm.clientApplication = vm.clientTest.clientProject.clientApplication;
			vm.clientAccount = vm.clientTest.clientProject.clientApplication.clientAccount;
		}

		if (vm.clientTest.id > 0) {
			loadClientApplications();
			loadClientProjects();
		}

		vm.datePickerOpenStatus = {};
		vm.openCalendar = openCalendar;
		vm.save = save;
		vm.loadClientApplications = loadClientApplications;
		vm.loadClientProjects = loadClientProjects;

		vm.clientaccounts = ClientAccount.query({
			filter : 'account_id-is-null'
		});

		$timeout(function() {
			angular.element('.form-group:eq(1)>input').focus();
		});

		function clear() {
			$uibModalInstance.dismiss('cancel');
		}

		function save() {
			vm.isSaving = true;
			if (vm.clientTest.id !== null) {
				ClientTest.update(vm.clientTest, onSaveSuccess, onSaveError);
			} else {
				ClientTest.save(vm.clientTest, onSaveSuccess, onSaveError);
			}
		}

		function loadClientApplications() {
			vm.clientApplications = [];
			vm.clientProjects = [];
			if (vm.clientAccount && vm.clientAccount.id) {
				vm.clientApplications = ClientApplication.query({
					accountId : vm.clientAccount.id
				});
			}
		}

		function loadClientProjects() {
			vm.clientProjects = [];
			if (vm.clientApplication && vm.clientApplication.id) {
				vm.clientProjects = ClientProject.query({
					applicationId : vm.clientApplication.id
				});
			}
		}

		function onSaveSuccess(result) {
			$scope.$emit('chartmanApp:clientTestUpdate', result);
			$uibModalInstance.close(result);
			vm.isSaving = false;
		}

		function onSaveError() {
			vm.isSaving = false;
		}

		vm.datePickerOpenStatus.testDate = false;

		function openCalendar(date) {
			vm.datePickerOpenStatus[date] = true;
		}
	}
})();
