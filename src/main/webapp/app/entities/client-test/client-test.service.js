(function() {
    'use strict';
    angular
        .module('chartmanApp')
        .factory('ClientTest', ClientTest);

    ClientTest.$inject = ['$resource', 'DateUtils'];

    function ClientTest ($resource, DateUtils) {
        var resourceUrl =  'api/client-tests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.testDate = DateUtils.convertDateTimeFromServer(data.testDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
