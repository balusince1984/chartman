(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('client-test', {
            parent: 'entity',
            url: '/client-test?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientTest.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-test/client-tests.html',
                    controller: 'ClientTestController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientTest');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('client-test-detail', {
            parent: 'entity',
            url: '/client-test/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'chartmanApp.clientTest.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/client-test/client-test-detail.html',
                    controller: 'ClientTestDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clientTest');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClientTest', function($stateParams, ClientTest) {
                    return ClientTest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'client-test',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('client-test-detail.edit', {
            parent: 'client-test-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-test/client-test-dialog.html',
                    controller: 'ClientTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientTest', function(ClientTest) {
                            return ClientTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-test.new', {
            parent: 'client-test',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-test/client-test-dialog.html',
                    controller: 'ClientTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                testName: null,
                                testDescription: null,
                                testDate: null,
                                active: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('client-test', null, { reload: 'client-test' });
                }, function() {
                    $state.go('client-test');
                });
            }]
        })
        .state('client-test.edit', {
            parent: 'client-test',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-test/client-test-dialog.html',
                    controller: 'ClientTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ClientTest', function(ClientTest) {
                            return ClientTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-test', null, { reload: 'client-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('client-test.delete', {
            parent: 'client-test',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/client-test/client-test-delete-dialog.html',
                    controller: 'ClientTestDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClientTest', function(ClientTest) {
                            return ClientTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('client-test', null, { reload: 'client-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
