(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('TestTransactionDetailController', TestTransactionDetailController);

    TestTransactionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TestTransaction'];

    function TestTransactionDetailController($scope, $rootScope, $stateParams, previousState, entity, TestTransaction) {
        var vm = this;

        vm.testTransaction = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('chartmanApp:testTransactionUpdate', function(event, result) {
            vm.testTransaction = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
