(function() {
    'use strict';

    angular
        .module('chartmanApp')
        .controller('TestTransactionDialogController', TestTransactionDialogController);

    TestTransactionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TestTransaction'];

    function TestTransactionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TestTransaction) {
        var vm = this;

        vm.testTransaction = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.testTransaction.id !== null) {
                TestTransaction.update(vm.testTransaction, onSaveSuccess, onSaveError);
            } else {
                TestTransaction.save(vm.testTransaction, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('chartmanApp:testTransactionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
