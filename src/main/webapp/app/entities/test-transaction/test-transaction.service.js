(function() {
    'use strict';
    angular
        .module('chartmanApp')
        .factory('TestTransaction', TestTransaction);

    TestTransaction.$inject = ['$resource'];

    function TestTransaction ($resource) {
        var resourceUrl =  'api/test-transactions/:clientTestId/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
