(function() {
	'use strict';

	angular.module('chartmanApp').controller('DashBoardController', DashBoardController);

	DashBoardController.$inject = [ '$state', 'id' ];

	function DashBoardController($state, id) {
		var vm = this;
		vm.id = id;
		vm.$state = $state;
		if ($state.current.name == "dashboard")
			$state.go('executiondetailsreport', {
				id : vm.id
			});
	}
})();
