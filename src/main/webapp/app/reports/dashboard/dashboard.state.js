(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('dashboard', {
			url : '/reporting/dashboard/{id}',
			parent : 'app',
			data : {
				authorities : [ 'ROLE_USER' ]
			},
			views : {
				'content@' : {
					templateUrl : 'app/reports/dashboard/dashboard.html',
					controller : 'DashBoardController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('dashboard');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				} ]
			}

		});
	}
})();
