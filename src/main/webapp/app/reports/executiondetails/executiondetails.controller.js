(function() {
	'use strict';

	angular.module('chartmanApp').controller('ExecutionDetailsReportingController', ExecutionDetailsReportingController);

	ExecutionDetailsReportingController.$inject = [ '$state', '$timeout', 'ExecutionDetails', 'id' ];

	function ExecutionDetailsReportingController($state, $timeout, ExecutionDetails, id) {
		var vm = this;
		var reports = [{
			id : "RESPONSETIME_SUMMARY_TABLE",
			title: "Transaction Response Time Summary"
		},			
		{
			id : "LOAD_HITS_PER_SECOND",
			title : "Hits Per Second"
		}, {
			id : "LOAD_THROUGHPUT",
			title : "Throughput (Bytes/sec)"
		}, {
			id : "LOAD_ERRORS_PER_SECOND",
			title : "Errors Per Second"
		}, {
			id : "LOAD_AVERAGE_RESPONSE_TIME",
			title : "Average Response Time (Seconds)"
		} ];

		for ( var r in reports) {
			
		 	(function(a, b) {
				ExecutionDetails.query({
					testId : a,
					graph : b.id
				},
				function(data) {
					 if(b.id=="RESPONSETIME_SUMMARY_TABLE"){
						 	vm.table = data;
                            var tablength = vm.table.length;
                            vm.table.sortType = 'transactioncode';
                            vm.table.sortReverse = false;
                            
					}else{
						var chartData = data;
						var chart = AmCharts.makeChart(b.id, {
						"type" : "serial",
						"plotAreaBorderAlpha" : 1.0,
						"plotAreaBorderColor" : "#838080",
						"theme" : "light",
						"marginTop" : 40,
						"marginRight" : 80,
						"synchronizeGrid" : true,
						"dataProvider" : chartData,
						"categoryAxis" : {
							"title" : "Elapsed Time (hh:mm:ss)",
							"labelFunction" : function(a, b) {
								return hhmmss(a);
							}
						},
						"chartCursor" : {
							"cursorPosition" : "mouse"
						},
						"valueAxes" : [ {
							"axisAlpha" : 0,
							"id" : "hitsPerSecondAxis",
							"position" : "left",
							"title" : b.title,
						}, {
							"axisAlpha" : 0,
							"id" : "userLoadAxis",
							"position" : "right",
							"title" : "User Load",
						} ],
						"graphs" : [ {
							"id" : "g1",
							"balloonText" : "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
							"bullet" : "round",
							"bulletSize" : 2,
							"bulletBorderThickness" : 1,
							"lineColor" : "#1ABB9C",
							"lineThickness" : 2,
							"valueAxis" : "hitsPerSecondAxis",
							"valueField" : "value",
							"title" : b.title
						}, {
							"id" : "g2",
							"balloonText" : "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
							"bullet" : "round",
							"bulletSize" : .5,
							"lineColor" : "#2A3F54 ",
							"bulletBorderThickness" : 1,
							"lineThickness" : 2,
							"valueAxis" : "userLoadAxis",
							"valueField" : "load",
							"title" : "User Load"
						} ],
						"legend" : {
							"useGraphSettings" : true
						},
						"categoryField" : "time",
						"export" : {
							"enabled" : true
						}
					});
				}

					
			});
			})(id, reports[r]);
		}
	}

	function pad(str) {
		return ("0" + str).slice(-2);
	}
	function hhmmss(secs) {
		var minutes = Math.floor(secs / 60);
		secs = secs % 60;
		var hours = Math.floor(minutes / 60)
		minutes = minutes % 60;
		return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
	}
})();
