(function() {
	'use strict';
	angular.module('chartmanApp').factory('ExecutionDetails', ExecutionDetails);

	ExecutionDetails.$inject = [ '$resource' ];

	function ExecutionDetails($resource) {
		var resourceUrl = 'api/reports/execsummary/:testId/:graph';

		return $resource(resourceUrl, {}, {
			'query' : {
				method : 'GET',
				isArray : true
			},
		});
	}
})();
