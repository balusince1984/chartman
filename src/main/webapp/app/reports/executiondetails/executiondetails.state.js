(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('executiondetailsreport', {
			parent : 'reportinghome',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/executiondetails',
			views : {
				'reportingtab' : {
					templateUrl : 'app/reports/executiondetails/reporting.html',
					controller : 'ExecutionDetailsReportingController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				} ]
			}
		});
	}
})();
