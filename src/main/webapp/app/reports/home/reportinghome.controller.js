(function() {
	'use strict';

	angular.module('chartmanApp').controller('ReportingHomeController', ReportingHomeController);

	ReportingHomeController.$inject = [ '$state', 'id' ];

	function ReportingHomeController($state, id) {
		var vm = this;
		vm.id = id;
		vm.$state = $state;
		if ($state.current.name == "reportinghome")
			$state.go('dashboarddetails', {
				id : vm.id
			});
	}
})();
