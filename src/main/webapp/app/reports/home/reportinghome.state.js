(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('reportinghome', {
			url : '/reporting/index/{id}',
			parent : 'app',
			data : {
				authorities : [ 'ROLE_USER' ]
			},
			views : {
				'content@' : {
					templateUrl : 'app/reports/home/index.html',
					controller : 'ReportingHomeController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('dashboard');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				} ]
			}

		});
	}
})();
