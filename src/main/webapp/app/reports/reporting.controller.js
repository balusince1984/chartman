(function() {
	'use strict';

	angular.module('chartmanApp').controller('ReportingController', ReportingController);

	ReportingController.$inject = [ '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', 'ClientAccount', 'ClientApplication', 'ClientProject', 'ClientTest' ];

	function ReportingController($state, Auth, Principal, ProfileService, LoginService, ClientAccount, ClientApplication, ClientProject, ClientTest) {
		var vm = this;
		vm.loadClientApplications = loadClientApplications;
		vm.loadClientProjects = loadClientProjects;
		vm.loadClientTests = loadClientTests;
		vm.loadDashboard = loadDashboard;
		vm.clientaccounts = ClientAccount.query({
			filter : 'account_id-is-null'
		});

		function loadClientApplications() {
			vm.clientApplications = [];
			if (vm.clientAccount && vm.clientAccount.id) {
				vm.clientApplications = ClientApplication.query({
					accountId : vm.clientAccount.id
				});
			}
		}

		function loadClientProjects() {
			vm.clientProjects = [];
			if (vm.clientApplication && vm.clientApplication.id) {
				vm.clientProjects = ClientProject.query({
					applicationId : vm.clientApplication.id
				});
			}
		}

		function loadClientTests() {
			vm.clientTests = [];
			if (vm.clientProject && vm.clientProject.id) {
				vm.clientTests = ClientTest.query({
					projectId : vm.clientProject.id
				});
			}
		}

		function loadDashboard() {
			if (vm.clientTest && vm.clientTest.id) {
				$state.go('reportinghome', {
					id : vm.clientTest.id
				});
			}
		}

	}
})();
