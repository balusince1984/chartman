(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);
	
	stateConfig.$inject = [ '$stateProvider' ];
	
	
	function stateConfig($stateProvider) {
		$stateProvider.state('reporting', {
			parent : 'app',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/reporting',
			views : {
				'content@' : {
					templateUrl : 'app/reports/reporting.html',
					controller : 'ReportingController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				
			}
		});
	}
})();
