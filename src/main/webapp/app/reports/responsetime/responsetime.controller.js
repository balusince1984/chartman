(function() {
	'use strict';

	angular.module('chartmanApp').controller('ResponseTimeReportingController', ResponseTimeReportingController);

	ResponseTimeReportingController.$inject = [ '$state', '$timeout', 'TestTransaction', 'ResponseTimeGraph', 'ResponseTime', 'ParseLinks', 'pagingParams', 'id' ];

	function ResponseTimeReportingController($state, $timeout, TestTransaction, ResponseTimeGraph, ResponseTime, ParseLinks, pagingParams, id) {
		var vm = this;
		vm.testId = id;

		vm.loadPage = loadPage;
		// vm.predicate = pagingParams.predicate;
		// vm.reverse = pagingParams.ascending;
		vm.transition = transition;
		vm.itemsPerPage = 3;
		loadAll();

		function loadAll() {
			TestTransaction.query({
				page : pagingParams.page - 1,
				size : vm.itemsPerPage
			}, onSuccess, onError);
			function onSuccess(data, headers) {
				vm.links = ParseLinks.parse(headers('link'));
				vm.totalItems = headers('X-Total-Count');
				vm.queryCount = vm.totalItems;
				vm.page = pagingParams.page;
				vm.testTransactions = data;
				$timeout(function() {
					loadUserLoad();
				});
			}
			function onError(error) {
				AlertService.error(error.data.message);
			}
		}

		function loadUserLoad() {
			ResponseTime.query({
				testId : vm.testId,
			}, function(data) {
				vm.userLoad = data;
				vm.userLoad.data.sort(function(a, b) {
					return a.ti - b.ti
				});
				generateGraph(Object.keys(vm.testTransactions), 0);
			});
		}

		function generateGraph(keys, index) {
			var t = vm.testTransactions[keys[index]];
			if (t) {
				ResponseTimeGraph.generateResponseTimeGraph(vm.testId, t.code, vm.userLoad);
				index++;
				generateGraph(keys, index);
			}
		}

		function loadPage(page) {
			vm.page = page;
			vm.transition();
		}

		function transition() {
			$state.transitionTo($state.$current, {
				page : vm.page,
				id : vm.testId
			});
		}
	}
})();
