(function() {
	'use strict';

	angular.module('chartmanApp').factory('ResponseTimeGraph', ResponseTimeGraph);

	ResponseTimeGraph.$inject = [ 'ResponseTime' ];

	function ResponseTimeGraph(ResponseTime) {
		var service = {
			generateResponseTimeGraph : generateResponseTimeGraph,
		};

		return service;

		function generateResponseTimeGraph(testId, transactionCode, userLoad, container) {
			ResponseTime.query({
				testId : testId,
				transactionCode : transactionCode
			}, function(data) {
				data.data.sort(function(a, b) {
					return a.ti - b.ti
				});
				container = container ? container : transactionCode;
				var chartData = mergeData(userLoad, data.data);
				var chart = AmCharts.makeChart(container, {
					"type" : "serial",
					"plotAreaBorderAlpha" : 1.0,
					"plotAreaBorderColor" : "#838080",
					"theme" : "light",
					"marginTop" : 40,
					"marginRight" : 80,
					"dataProvider" : chartData,
					"synchronizeGrid" : true,
					"categoryAxis" : {
						"title" : "Elapsed Time (hh:mm:ss)"
					},
					"chartCursor" : {
						"cursorPosition" : "mouse"
					},
					"valueAxes" : [ {
						"axisAlpha" : 0,
						"id" : "userLoadAxis",
						"position" : "right",
						"title" : "User Load",
					}, {
						"axisAlpha" : 0,
						"id" : "responseTimeAxis",
						"position" : "left",
						"title" : "Response Time (Seconds)"
					} ],
					"graphs" : [ {
						"id" : "g1",
						"balloonText" : "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
						"bullet" : "round",
						"bulletSize" : 2,
						"lineColor" : "#1ABB9C",
						"lineThickness" : 2,
						"valueAxis" : "responseTimeAxis",
						"valueField" : "responseTime",
						"title" : "Response Time (Seconds)"
					}, {
						"id" : "g2",
						"balloonText" : "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
						"bullet" : "round",
						"bulletSize" : .5,
						"lineColor" : "#2A3F54 ",
						"lineThickness" : 2,
						/* "type" : "smoothedLine", */
						"valueAxis" : "userLoadAxis",
						"valueField" : "allThreads",
						"title" : "User Load"
					} ],
					"legend" : {
						"useGraphSettings" : true
					},
					"categoryField" : "timeIndex",
					"export" : {
						"enabled" : true
					}
				});
			}, function(error) {
				console.log(error);
			});
			
			function mergeData(userLoad, responseTime) {
				var output = [];
				var t = 0;
				for (var load = 0; load < userLoad.data.length; load++) {
					if (responseTime[t] && responseTime[t].ti == userLoad.data[load].ti) {
						output.push({
							"timeIndex" : hhmmss(userLoad.data[load].ti),
							"allThreads" : userLoad.data[load].ul,
							"responseTime" : Math.floor(responseTime[t].rt / 1000),
						});
						t = t + 1;
					} else {
						output.push({
							"timeIndex" : hhmmss(userLoad.data[load].ti),
							"allThreads" : userLoad.data[load].ul,
							"responseTime" : null,
						});
					}
				}
				return output;
			}

			function pad(str) {
				return ("0" + str).slice(-2);
			}
			function hhmmss(secs) {
				var minutes = Math.floor(secs / 60);
				secs = secs % 60;
				var hours = Math.floor(minutes / 60)
				minutes = minutes % 60;
				return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
			}
		}
	}
})();
