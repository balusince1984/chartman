(function() {
	'use strict';
	angular.module('chartmanApp').factory('ResponseTime', ResponseTime);
	ResponseTime.$inject = [ '$resource' ];

	function ResponseTime($resource) {
		var resourceUrl = 'api/reports/responsetimetrend/:testId/:transactionCode';
		return $resource(resourceUrl, {}, {
			'query' : {
				method : 'GET'
			},
		});
	}
})();
