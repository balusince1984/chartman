(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);
	
	stateConfig.$inject = [ '$stateProvider' ];

	/*function stateConfig($stateProvider) {
		$stateProvider.state('responsetimereport', {
			parent : 'reportinghome',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/responsetime',
			views : {
				'reportingtab' : {
					templateUrl : 'app/reports/responsetime/reporting.html',
					controller : 'ResponseTimeReportingController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				responseTimeChartData : ['$stateParams','ResponseTimeChartData',function($stateParams,ResponseTimeChartData){
					return ResponseTimeChartData.query({
						testId: $stateParams.id,
						transactionCode: $stateParams.transactionCode
					});
				}],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				} ]
			}
		});
	}*/
	function stateConfig($stateProvider) {
		$stateProvider.state('responsetimechartdata', {
			parent : 'reportinghome',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/responsetimechartdata',
			//url : '/responsetime',
			views : {
				'reportingtab' : {
					templateUrl : 'app/reports/responsetime/reporting.html',
					controller : 'ResponseTimeReportingController',
					controllerAs : 'vm'
				}
			},
			params :{
				page: {
                    value: '1',
                    squash: true
                }
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				} ],
				pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
	                    return {
	                        page: PaginationUtil.parsePage($stateParams.page),
	                       // sort: $stateParams.sort,
	                       // predicate: PaginationUtil.parsePredicate($stateParams.sort),
	                        //ascending: PaginationUtil.parseAscending($stateParams.sort),
	                        //search: $stateParams.search
	                    };
	                }],
			}
		});
	}
})();
