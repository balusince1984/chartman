(function() {
	'use strict';

	angular.module('chartmanApp').controller('ResultsDashboardController', ResultsDashboardController);

	ResultsDashboardController.$inject = [ '$state', '$timeout', 'DashboardDetails', 'id' ];

	function ResultsDashboardController($state, $timeout, DashboardDetails, id) {
		var vm = this;
		var reports = [ {
			id : "DASH_BOARD_METRICS",
			title : "Summary of Performance metrics"
		}, {
			id : "SLOW_TRANSACTIONS",
			title : "Top 5 Slow transaction Response times"
		}, {
			id : "TRANSACTION_RESULTS",
			title : "Transaction Success vs Failed Distribution"
		}, {
			id : "SLA_RESULTS",
			title : "Transaction SLA - Fullfill vs Failed Distribution"
		}, {
			id : "RESPONSE_TIME_DISTRIBUTION",
			title : "Transaction resonse time distributions"
		}, {
			id : "HTTP_RESPONSE_CODES",
			title : "HTTP Response codes distribution"
		}];

		for (var k in reports) {

			(function(a, b) {
				if(b === "SLOW_TRANSACTIONS" || b === "HTTP_RESPONSE_CODES"){
					DashboardDetails.queryForList({
						testId : a,
						graph : b
					}, function(data) {
						generateDashboard(data, b);
					})
				}else{
					DashboardDetails.queryForObject({
						testId : a,
						graph : b
					}, function(data) {
						generateDashboard(data, b);
					})
				}
				
			})(id, reports[k].id);
		}
		
		function generateDashboard(data, reportId){
			if(reportId === "DASH_BOARD_METRICS"){
				vm.mapdata = data;
			}
			else if(reportId === "SLOW_TRANSACTIONS"){
				vm.tdata = data;
				vm.tdata.sortType = 'avgresponsetime';
				vm.tdata.sortReverse = true;
			}
			else if (reportId === "TRANSACTION_RESULTS") {
				var mapdata = data;
				var chartData = [];
				//var tc = mapdata.transactionStatusCount;
				for ( var key in mapdata) {
					if (mapdata.hasOwnProperty(key)) {
						if (key === 'totalCount') {
							// nothing
						} else if (key === "successCount") {
							chartData.push({
								type : "Successful Transactions",
								count : mapdata[key],
								color : "#1ABB9C"
							// pulled: true
							});
						} else if (key === "failedCount") {
							chartData.push({
								type : "Failed Transactions",
								count : mapdata[key],
								color : "#2A3F54"
							// pulled: true
							});
						}
					}
				}

				var chart = AmCharts.makeChart("id-trans-status-count", {
					"type" : "pie",
					"theme" : "light",
					"dataProvider" : chartData,
					"valueField" : "count",
					"titleField" : "type",
					"colorField" : "color",
					"outlineColor" : "#FFFFFF",
					"outlineAlpha" : 0.6,
					"outlineThickness" : 2,
					"radius" : "30%",
					"innerRadius" : "25%",
					"legend" : {
						"position" : "bottom",
						"marginRight" : 100,
						"autoMargins" : false
					},
					"balloon" : {
						"fixedPosition" : true
					},
					"export" : {
						"enabled" : true
					}
				});
			} else if (reportId === "SLA_RESULTS") {
				var mapdata = data;
				var chartData1 = [];
				//var tc1 = mapdata.responseTimeSlaDistribution;
				for ( var key1 in mapdata) {
					if (mapdata.hasOwnProperty(key1)) {
						if (key1 === "below") {
							chartData1.push({
								x : "Transactions Below SLA",
								y : mapdata[key1],
								color : "#1ABB9C"
							// pulled: true
							});
						} else if (key1 === "above") {
							chartData1.push({
								x : "Transactions Failed to Meet SLA",
								y : mapdata[key1],
								color : "#2A3F54"
							// pulled: true
							});
						}
					}
				}
				var chart = AmCharts.makeChart("id-trans-sla-count", {

					"type" : "pie",
					"theme" : "light",
					"dataProvider" : chartData1,
					"valueField" : "y",
					"titleField" : "x",
					"colorField" : "color",
					"outlineColor" : "#FFFFFF",
					"outlineAlpha" : 0.6,
					"outlineThickness" : 2,
					"radius" : "30%",
					"innerRadius" : "25%",
					"legend" : {
						"position" : "bottom",
						"marginRight" : 100,
						"autoMargins" : false
					},
					"balloon" : {
						"fixedPosition" : true
					},
					"export" : {
						"enabled" : true
					}
				});
			} else if (reportId === "RESPONSE_TIME_DISTRIBUTION") {
				var mapdata = data;
				var chartData2 = [];
				//var tc1 = mapdata.transResponseTimeDistribution;
				for ( var key1 in mapdata) {
					if (mapdata.hasOwnProperty(key1)) {
						if (key1 === "rangeOne") {
							chartData2.push({
								x : "<= 1 Second",
								y : mapdata[key1],
								"color" : "#1ABB9C"
							// pulled: true
							});
						} else if (key1 === "rangeTwo") {
							chartData2.push({
								x : "Between 1 Second and 5 Seconds",
								y : mapdata[key1],
								"color" : "#73879C"
							// pulled: true
							});
						} else if (key1 === "rangeThree") {
							chartData2.push({
								x : "> 5 Seconds",
								y : mapdata[key1],
								"color" : "#2A3F54"
							// pulled: true
							});
						}
					}
				}

				var chart = AmCharts.makeChart("id-trans-response-dist", {
					"type" : "pie",
					"theme" : "light",
					"dataProvider" : chartData2,
					"valueField" : "y",
					"titleField" : "x",
					"colorField" : "color",
					"outlineColor" : "#FFFFFF",
					"outlineAlpha" : 0.6,
					"outlineThickness" : 2,
					"radius" : "30%",
					"innerRadius" : "25%",
					"legend" : {
						"position" : "bottom",
						"marginRight" : 100,
						"autoMargins" : false
					},
					"balloon" : {
						"fixedPosition" : true
					},
					"export" : {
						"enabled" : true
					}
				});
			} else if (reportId === "HTTP_RESPONSE_CODES") {
				
				var totalCount = 0;
				var j = 0;
				var tempObject = data;
				vm.mapdata1 = data;
				for ( var rType in tempObject) {
					if(tempObject[rType].count){
						totalCount = totalCount + tempObject[rType].count;
					}
				}
				for ( var sType in tempObject) {
					if(tempObject[sType].count){
						tempObject[sType].percentage = new Number((tempObject[sType].count * 100) / totalCount).toFixed(2);
					}
					
				}
			}
		}
	}
})();
