(function() {
	'use strict';
	angular.module('chartmanApp').factory('DashboardDetails', DashboardDetails);
	DashboardDetails.$inject = [ '$resource' ];
	
	function DashboardDetails($resource) {
		var resourceUrl = 'api/reports/resultsdashboard/:testId/:graph';
		
		return $resource(resourceUrl, {}, {
				'queryForList' : {
					method : 'GET',
					isArray : true
				},
				'queryForObject' : {
					method : 'GET',
					isArray : false
				},
		});
	}
})();