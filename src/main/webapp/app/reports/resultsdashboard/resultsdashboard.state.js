(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('dashboarddetails', {
			parent : 'reportinghome',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/dashboarddetails',
			views : {
				'reportingtab' : {
					templateUrl : 'app/reports/resultsdashboard/resultsdashboard.html',
					controller : 'ResultsDashboardController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				}] 
				
			}
		});
	}
})();
