(function() {
	'use strict';

	angular.module('chartmanApp').controller('TransactionDetailsController', TransactionDetailsController);

	TransactionDetailsController.$inject = [ '$state', '$timeout', 'TransactionDetails', 'ResponseTime', 'id', 'transactionCode', 'transactionList', 'ResponseTimeGraph' ];

	function TransactionDetailsController($state, $timeout, TransactionDetails, ResponseTime, id, transactionCode, transactionList, ResponseTimeGraph) {
		var vm = this;
		vm.reloadTransactionDetails = reloadTransactionDetails;
		var reports = [ {
			id : "TREND_CHART",
			title : "Transaction Response time trend chart"
		}, {
			id : "TRANSACTION_RESULTS",
			title : "Transaction Success vs Failed Distribution"
		}, {
			id : "SLA_RESULTS",
			title : "Transaction SLA - Fullfill vs Failed Distribution"
		}, {
			id : "RESPONSE_TIME_DISTRIBUTION",
			title : "Transaction resonse time distributions"
		}, {
			id : "HTTP_RESPONSE_CODES",
			title : "HTTP Response codes distribution"
		}, {
			id : "TRANSACTION_SUMMARY",
			title : "Transaction Summary Details"
		} ];

		vm.transactionCode = transactionCode;
		vm.transactionList = transactionList;
		vm.transactionOk = vm.transactionCode != null && vm.transactionCode.length > 0;

		function reloadTransactionDetails() {
			vm.transactionOk = vm.transactionCode != null && vm.transactionCode.length > 0;
			if (vm.transactionOk) {
				$state.go('transactiondetails', {
					transactionCode : vm.transactionCode
				});
			}
		}
		vm.transactionCode && (function(a, b, c) {
			TransactionDetails.query({
				testId : a,
				transactionCode : c,

			}, function(data) {
				for ( var r in b) {
					vm.mapdata = data;
					vm.tdata = vm.mapdata.transactionSummary;
					if (b[r].id === "TREND_CHART") {
						ResponseTime.query({
							testId : a,
						}, function(userLoad) {
							userLoad.data.sort(function(a, b) {
								return a.ti - b.ti
							});
							ResponseTimeGraph.generateResponseTimeGraph(a, c, userLoad);
						});
					} else if (b[r].id === "TRANSACTION_RESULTS") {
						var mapdata = data;
						var chartData = [];
						var tc = mapdata.transactionStatusCount;
						for ( var key in tc) {
							if (tc.hasOwnProperty(key)) {
								if (key === 'totalCount') {
									// nothing
								} else if (key === "successCount") {
									chartData.push({
										type : "Successful Transactions",
										count : tc[key],
										color : "#1ABB9C"
									// pulled: true
									});
								} else if (key === "failedCount") {
									chartData.push({
										type : "Failed Transactions",
										count : tc[key],
										color : "#2A3F54"
									// pulled: true
									});
								}
							}
						}

						var chart = AmCharts.makeChart("id-trans-status-count", {
							"type" : "pie",
							"theme" : "light",
							"dataProvider" : chartData,
							"valueField" : "count",
							"titleField" : "type",
							"colorField" : "color",
							"outlineColor" : "#FFFFFF",
							"outlineAlpha" : 0.6,
							"outlineThickness" : 2,
							"radius" : "30%",
							"innerRadius" : "25%",
							"legend" : {
								"position" : "bottom",
								"marginRight" : 100,
								"autoMargins" : false
							},
							"balloon" : {
								"fixedPosition" : true
							},
							"export" : {
								"enabled" : true
							}
						});
					} else if (b[r].id === "SLA_RESULTS") {
						var mapdata = data;
						var chartData1 = [];
						var tc1 = mapdata.responseTimeSlaDistribution;
						for ( var key1 in tc1) {
							if (tc1.hasOwnProperty(key1)) {
								if (key1 === "below") {
									chartData1.push({
										x : "Transactions Below SLA",
										y : tc1[key1],
										color : "#1ABB9C"
									// pulled: true
									});
								} else if (key1 === "above") {
									chartData1.push({
										x : "Transactions Failed to Meet SLA",
										y : tc1[key1],
										color : "#2A3F54"
									// pulled: true
									});
								}
							}
						}
						var chart = AmCharts.makeChart("id-trans-sla-count", {

							"type" : "pie",
							"theme" : "light",
							"dataProvider" : chartData1,
							"valueField" : "y",
							"titleField" : "x",
							"colorField" : "color",
							"outlineColor" : "#FFFFFF",
							"outlineAlpha" : 0.6,
							"outlineThickness" : 2,
							"radius" : "30%",
							"innerRadius" : "25%",
							"legend" : {
								"position" : "bottom",
								"marginRight" : 100,
								"autoMargins" : false
							},
							"balloon" : {
								"fixedPosition" : true
							},
							"export" : {
								"enabled" : true
							}
						});
					} else if (b[r].id === "RESPONSE_TIME_DISTRIBUTION") {
						var mapdata = data;
						var chartData2 = [];
						var tc1 = mapdata.transResponseTimeDistribution;
						for ( var key1 in tc1) {
							if (tc1.hasOwnProperty(key1)) {
								if (key1 === "rangeOne") {
									chartData2.push({
										x : "<= 1 Second",
										y : tc1[key1],
										"color" : "#1ABB9C"
									// pulled: true
									});
								} else if (key1 === "rangeTwo") {
									chartData2.push({
										x : "Between 1 Second and 5 Seconds",
										y : tc1[key1],
										"color" : "#73879C"
									// pulled: true
									});
								} else if (key1 === "rangeThree") {
									chartData2.push({
										x : "> 5 Seconds",
										y : tc1[key1],
										"color" : "#2A3F54"
									// pulled: true
									});
								}
							}
						}

						var chart = AmCharts.makeChart("id-trans-response-dist", {
							"type" : "pie",
							"theme" : "light",
							"dataProvider" : chartData2,
							"valueField" : "y",
							"titleField" : "x",
							"colorField" : "color",
							"outlineColor" : "#FFFFFF",
							"outlineAlpha" : 0.6,
							"outlineThickness" : 2,
							"radius" : "30%",
							"innerRadius" : "25%",
							"legend" : {
								"position" : "bottom",
								"marginRight" : 100,
								"autoMargins" : false
							},
							"balloon" : {
								"fixedPosition" : true
							},
							"export" : {
								"enabled" : true
							}
						});
					} else if (b[r].id === "HTTP_RESPONSE_CODES") {
						var mapdata = data;
						var totalCount = 0;
						var tempObject = mapdata.httpResponseDistributionList;
						for ( var rType in tempObject) {
							totalCount = totalCount + tempObject[rType].count;
						}
						for ( var sType in tempObject) {
							tempObject[sType].percentage = new Number((tempObject[sType].count * 100) / totalCount).toFixed(2);

						}
					}
				}

			});
		})(id, reports, transactionCode);
	}
})();
