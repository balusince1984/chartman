(function() {
	'use strict';
	angular.module('chartmanApp').factory('TransactionDetails', TransactionDetails);
	angular.module('chartmanApp').factory('TransactionList', TransactionList);

	TransactionList.$inject = [ '$resource' ];
	TransactionDetails.$inject = [ '$resource' ];
	
	function TransactionDetails($resource) {
		var resourceUrl = 'api/reports/transactiondetails/:testId/:transactionCode';

		return $resource(resourceUrl, {}, {
			'query' : {
				method : 'GET',
				isArray : false
			},
		});
	}

	function TransactionList($resource) {
		var resourceUrl = 'api/reports/transactionlist/:testId';

		return $resource(resourceUrl, {}, {
			'query' : {
				method : 'GET',
				isArray : true
			},
		});
	}

})();
