(function() {
	'use strict';

	angular.module('chartmanApp').config(stateConfig);

	stateConfig.$inject = [ '$stateProvider' ];

	function stateConfig($stateProvider) {
		$stateProvider.state('transactiondetails', {
			parent : 'reportinghome',
			data : {
				authorities : [ 'ROLE_USER' ],
				pageTitle : 'chartmanApp.clientAccount.home.title'
			},
			url : '/transactiondetails/{transactionCode}',
			views : {
				'reportingtab' : {
					templateUrl : 'app/reports/transactiondetails/transactiondetails.html',
					controller : 'TransactionDetailsController',
					controllerAs : 'vm'
				}
			},
			resolve : {
				translatePartialLoader : [ '$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('reporting');
					$translatePartialLoader.addPart('global');
					return $translate.refresh();
				} ],
				id : [ '$stateParams', function($stateParams) {
					return $stateParams.id;
				}],
				transactionCode : [ '$stateParams', function($stateParams) {
					return $stateParams.transactionCode;
				}],
				transactionList : ['$stateParams','TransactionList',function($stateParams,TransactionList){
					return TransactionList.query({
						testId: $stateParams.id,
					});
				}],
				/*responseTimeChartData : ['$stateParams','ResponseTimeChartData',function($stateParams,ResponseTimeChartData){
					return ResponseTimeChartData.query({
						testId: $stateParams.id,
						transactionCode: $stateParams.transactionCode
					});
				}]*/
				
			}
		})
	}
})();
