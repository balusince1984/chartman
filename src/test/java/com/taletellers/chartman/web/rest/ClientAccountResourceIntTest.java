package com.taletellers.chartman.web.rest;

import com.taletellers.chartman.ChartmanApp;

import com.taletellers.chartman.domain.ClientAccount;
import com.taletellers.chartman.repository.ClientAccountRepository;
import com.taletellers.chartman.service.ClientAccountService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientAccountResource REST controller.
 *
 * @see ClientAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChartmanApp.class)
public class ClientAccountResourceIntTest {

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACCOUNT_STATUS = false;
    private static final Boolean UPDATED_ACCOUNT_STATUS = true;

    @Inject
    private ClientAccountRepository clientAccountRepository;

    @Inject
    private ClientAccountService clientAccountService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restClientAccountMockMvc;

    private ClientAccount clientAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClientAccountResource clientAccountResource = new ClientAccountResource();
        ReflectionTestUtils.setField(clientAccountResource, "clientAccountService", clientAccountService);
        this.restClientAccountMockMvc = MockMvcBuilders.standaloneSetup(clientAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientAccount createEntity(EntityManager em) {
        ClientAccount clientAccount = new ClientAccount()
                .accountName(DEFAULT_ACCOUNT_NAME)
                .accountDescription(DEFAULT_ACCOUNT_DESCRIPTION)
                .accountStatus(DEFAULT_ACCOUNT_STATUS);
        return clientAccount;
    }

    @Before
    public void initTest() {
        clientAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientAccount() throws Exception {
        int databaseSizeBeforeCreate = clientAccountRepository.findAll().size();

        // Create the ClientAccount

        restClientAccountMockMvc.perform(post("/api/client-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAccount)))
            .andExpect(status().isCreated());

        // Validate the ClientAccount in the database
        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeCreate + 1);
        ClientAccount testClientAccount = clientAccountList.get(clientAccountList.size() - 1);
        assertThat(testClientAccount.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testClientAccount.getAccountDescription()).isEqualTo(DEFAULT_ACCOUNT_DESCRIPTION);
        assertThat(testClientAccount.isAccountStatus()).isEqualTo(DEFAULT_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void createClientAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientAccountRepository.findAll().size();

        // Create the ClientAccount with an existing ID
        ClientAccount existingClientAccount = new ClientAccount();
        existingClientAccount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientAccountMockMvc.perform(post("/api/client-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingClientAccount)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAccountNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientAccountRepository.findAll().size();
        // set the field null
        clientAccount.setAccountName(null);

        // Create the ClientAccount, which fails.

        restClientAccountMockMvc.perform(post("/api/client-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAccount)))
            .andExpect(status().isBadRequest());

        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientAccounts() throws Exception {
        // Initialize the database
        clientAccountRepository.saveAndFlush(clientAccount);

        // Get all the clientAccountList
        restClientAccountMockMvc.perform(get("/api/client-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME.toString())))
            .andExpect(jsonPath("$.[*].accountDescription").value(hasItem(DEFAULT_ACCOUNT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].accountStatus").value(hasItem(DEFAULT_ACCOUNT_STATUS.booleanValue())));
    }

    @Test
    @Transactional
    public void getClientAccount() throws Exception {
        // Initialize the database
        clientAccountRepository.saveAndFlush(clientAccount);

        // Get the clientAccount
        restClientAccountMockMvc.perform(get("/api/client-accounts/{id}", clientAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientAccount.getId().intValue()))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME.toString()))
            .andExpect(jsonPath("$.accountDescription").value(DEFAULT_ACCOUNT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.accountStatus").value(DEFAULT_ACCOUNT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClientAccount() throws Exception {
        // Get the clientAccount
        restClientAccountMockMvc.perform(get("/api/client-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientAccount() throws Exception {
        // Initialize the database
        clientAccountService.save(clientAccount);

        int databaseSizeBeforeUpdate = clientAccountRepository.findAll().size();

        // Update the clientAccount
        ClientAccount updatedClientAccount = clientAccountRepository.findOne(clientAccount.getId());
        updatedClientAccount
                .accountName(UPDATED_ACCOUNT_NAME)
                .accountDescription(UPDATED_ACCOUNT_DESCRIPTION)
                .accountStatus(UPDATED_ACCOUNT_STATUS);

        restClientAccountMockMvc.perform(put("/api/client-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientAccount)))
            .andExpect(status().isOk());

        // Validate the ClientAccount in the database
        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeUpdate);
        ClientAccount testClientAccount = clientAccountList.get(clientAccountList.size() - 1);
        assertThat(testClientAccount.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testClientAccount.getAccountDescription()).isEqualTo(UPDATED_ACCOUNT_DESCRIPTION);
        assertThat(testClientAccount.isAccountStatus()).isEqualTo(UPDATED_ACCOUNT_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingClientAccount() throws Exception {
        int databaseSizeBeforeUpdate = clientAccountRepository.findAll().size();

        // Create the ClientAccount

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientAccountMockMvc.perform(put("/api/client-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientAccount)))
            .andExpect(status().isCreated());

        // Validate the ClientAccount in the database
        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientAccount() throws Exception {
        // Initialize the database
        clientAccountService.save(clientAccount);

        int databaseSizeBeforeDelete = clientAccountRepository.findAll().size();

        // Get the clientAccount
        restClientAccountMockMvc.perform(delete("/api/client-accounts/{id}", clientAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientAccount> clientAccountList = clientAccountRepository.findAll();
        assertThat(clientAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
