package com.taletellers.chartman.web.rest;

import com.taletellers.chartman.ChartmanApp;

import com.taletellers.chartman.domain.ClientApplication;
import com.taletellers.chartman.repository.ClientApplicationRepository;
import com.taletellers.chartman.service.ClientApplicationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientApplicationResource REST controller.
 *
 * @see ClientApplicationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChartmanApp.class)
public class ClientApplicationResourceIntTest {

    private static final String DEFAULT_APPLICATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_APPLICATION_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_APPLICATION_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Inject
    private ClientApplicationRepository clientApplicationRepository;

    @Inject
    private ClientApplicationService clientApplicationService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restClientApplicationMockMvc;

    private ClientApplication clientApplication;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClientApplicationResource clientApplicationResource = new ClientApplicationResource();
        ReflectionTestUtils.setField(clientApplicationResource, "clientApplicationService", clientApplicationService);
        this.restClientApplicationMockMvc = MockMvcBuilders.standaloneSetup(clientApplicationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientApplication createEntity(EntityManager em) {
        ClientApplication clientApplication = new ClientApplication()
                .applicationName(DEFAULT_APPLICATION_NAME)
                .applicationDescription(DEFAULT_APPLICATION_DESCRIPTION)
                .active(DEFAULT_ACTIVE);
        return clientApplication;
    }

    @Before
    public void initTest() {
        clientApplication = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientApplication() throws Exception {
        int databaseSizeBeforeCreate = clientApplicationRepository.findAll().size();

        // Create the ClientApplication

        restClientApplicationMockMvc.perform(post("/api/client-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientApplication)))
            .andExpect(status().isCreated());

        // Validate the ClientApplication in the database
        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeCreate + 1);
        ClientApplication testClientApplication = clientApplicationList.get(clientApplicationList.size() - 1);
        assertThat(testClientApplication.getApplicationName()).isEqualTo(DEFAULT_APPLICATION_NAME);
        assertThat(testClientApplication.getApplicationDescription()).isEqualTo(DEFAULT_APPLICATION_DESCRIPTION);
        assertThat(testClientApplication.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createClientApplicationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientApplicationRepository.findAll().size();

        // Create the ClientApplication with an existing ID
        ClientApplication existingClientApplication = new ClientApplication();
        existingClientApplication.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientApplicationMockMvc.perform(post("/api/client-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingClientApplication)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkApplicationNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientApplicationRepository.findAll().size();
        // set the field null
        clientApplication.setApplicationName(null);

        // Create the ClientApplication, which fails.

        restClientApplicationMockMvc.perform(post("/api/client-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientApplication)))
            .andExpect(status().isBadRequest());

        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientApplications() throws Exception {
        // Initialize the database
        clientApplicationRepository.saveAndFlush(clientApplication);

        // Get all the clientApplicationList
        restClientApplicationMockMvc.perform(get("/api/client-applications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientApplication.getId().intValue())))
            .andExpect(jsonPath("$.[*].applicationName").value(hasItem(DEFAULT_APPLICATION_NAME.toString())))
            .andExpect(jsonPath("$.[*].applicationDescription").value(hasItem(DEFAULT_APPLICATION_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getClientApplication() throws Exception {
        // Initialize the database
        clientApplicationRepository.saveAndFlush(clientApplication);

        // Get the clientApplication
        restClientApplicationMockMvc.perform(get("/api/client-applications/{id}", clientApplication.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientApplication.getId().intValue()))
            .andExpect(jsonPath("$.applicationName").value(DEFAULT_APPLICATION_NAME.toString()))
            .andExpect(jsonPath("$.applicationDescription").value(DEFAULT_APPLICATION_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClientApplication() throws Exception {
        // Get the clientApplication
        restClientApplicationMockMvc.perform(get("/api/client-applications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientApplication() throws Exception {
        // Initialize the database
        clientApplicationService.save(clientApplication);

        int databaseSizeBeforeUpdate = clientApplicationRepository.findAll().size();

        // Update the clientApplication
        ClientApplication updatedClientApplication = clientApplicationRepository.findOne(clientApplication.getId());
        updatedClientApplication
                .applicationName(UPDATED_APPLICATION_NAME)
                .applicationDescription(UPDATED_APPLICATION_DESCRIPTION)
                .active(UPDATED_ACTIVE);

        restClientApplicationMockMvc.perform(put("/api/client-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientApplication)))
            .andExpect(status().isOk());

        // Validate the ClientApplication in the database
        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeUpdate);
        ClientApplication testClientApplication = clientApplicationList.get(clientApplicationList.size() - 1);
        assertThat(testClientApplication.getApplicationName()).isEqualTo(UPDATED_APPLICATION_NAME);
        assertThat(testClientApplication.getApplicationDescription()).isEqualTo(UPDATED_APPLICATION_DESCRIPTION);
        assertThat(testClientApplication.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingClientApplication() throws Exception {
        int databaseSizeBeforeUpdate = clientApplicationRepository.findAll().size();

        // Create the ClientApplication

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientApplicationMockMvc.perform(put("/api/client-applications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientApplication)))
            .andExpect(status().isCreated());

        // Validate the ClientApplication in the database
        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientApplication() throws Exception {
        // Initialize the database
        clientApplicationService.save(clientApplication);

        int databaseSizeBeforeDelete = clientApplicationRepository.findAll().size();

        // Get the clientApplication
        restClientApplicationMockMvc.perform(delete("/api/client-applications/{id}", clientApplication.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientApplication> clientApplicationList = clientApplicationRepository.findAll();
        assertThat(clientApplicationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
