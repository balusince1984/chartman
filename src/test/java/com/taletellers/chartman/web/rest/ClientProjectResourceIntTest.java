package com.taletellers.chartman.web.rest;

import com.taletellers.chartman.ChartmanApp;

import com.taletellers.chartman.domain.ClientProject;
import com.taletellers.chartman.repository.ClientProjectRepository;
import com.taletellers.chartman.service.ClientProjectService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientProjectResource REST controller.
 *
 * @see ClientProjectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChartmanApp.class)
public class ClientProjectResourceIntTest {

    private static final String DEFAULT_PROJECT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROJECT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PROJECT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_PROJECT_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Inject
    private ClientProjectRepository clientProjectRepository;

    @Inject
    private ClientProjectService clientProjectService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restClientProjectMockMvc;

    private ClientProject clientProject;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClientProjectResource clientProjectResource = new ClientProjectResource();
        ReflectionTestUtils.setField(clientProjectResource, "clientProjectService", clientProjectService);
        this.restClientProjectMockMvc = MockMvcBuilders.standaloneSetup(clientProjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientProject createEntity(EntityManager em) {
        ClientProject clientProject = new ClientProject()
                .projectName(DEFAULT_PROJECT_NAME)
                .projectDescription(DEFAULT_PROJECT_DESCRIPTION)
                .active(DEFAULT_ACTIVE);
        return clientProject;
    }

    @Before
    public void initTest() {
        clientProject = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientProject() throws Exception {
        int databaseSizeBeforeCreate = clientProjectRepository.findAll().size();

        // Create the ClientProject

        restClientProjectMockMvc.perform(post("/api/client-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProject)))
            .andExpect(status().isCreated());

        // Validate the ClientProject in the database
        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeCreate + 1);
        ClientProject testClientProject = clientProjectList.get(clientProjectList.size() - 1);
        assertThat(testClientProject.getProjectName()).isEqualTo(DEFAULT_PROJECT_NAME);
        assertThat(testClientProject.getProjectDescription()).isEqualTo(DEFAULT_PROJECT_DESCRIPTION);
        assertThat(testClientProject.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createClientProjectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientProjectRepository.findAll().size();

        // Create the ClientProject with an existing ID
        ClientProject existingClientProject = new ClientProject();
        existingClientProject.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientProjectMockMvc.perform(post("/api/client-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingClientProject)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkProjectNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProjectRepository.findAll().size();
        // set the field null
        clientProject.setProjectName(null);

        // Create the ClientProject, which fails.

        restClientProjectMockMvc.perform(post("/api/client-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProject)))
            .andExpect(status().isBadRequest());

        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientProjects() throws Exception {
        // Initialize the database
        clientProjectRepository.saveAndFlush(clientProject);

        // Get all the clientProjectList
        restClientProjectMockMvc.perform(get("/api/client-projects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientProject.getId().intValue())))
            .andExpect(jsonPath("$.[*].projectName").value(hasItem(DEFAULT_PROJECT_NAME.toString())))
            .andExpect(jsonPath("$.[*].projectDescription").value(hasItem(DEFAULT_PROJECT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getClientProject() throws Exception {
        // Initialize the database
        clientProjectRepository.saveAndFlush(clientProject);

        // Get the clientProject
        restClientProjectMockMvc.perform(get("/api/client-projects/{id}", clientProject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientProject.getId().intValue()))
            .andExpect(jsonPath("$.projectName").value(DEFAULT_PROJECT_NAME.toString()))
            .andExpect(jsonPath("$.projectDescription").value(DEFAULT_PROJECT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClientProject() throws Exception {
        // Get the clientProject
        restClientProjectMockMvc.perform(get("/api/client-projects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientProject() throws Exception {
        // Initialize the database
        clientProjectService.save(clientProject);

        int databaseSizeBeforeUpdate = clientProjectRepository.findAll().size();

        // Update the clientProject
        ClientProject updatedClientProject = clientProjectRepository.findOne(clientProject.getId());
        updatedClientProject
                .projectName(UPDATED_PROJECT_NAME)
                .projectDescription(UPDATED_PROJECT_DESCRIPTION)
                .active(UPDATED_ACTIVE);

        restClientProjectMockMvc.perform(put("/api/client-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientProject)))
            .andExpect(status().isOk());

        // Validate the ClientProject in the database
        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeUpdate);
        ClientProject testClientProject = clientProjectList.get(clientProjectList.size() - 1);
        assertThat(testClientProject.getProjectName()).isEqualTo(UPDATED_PROJECT_NAME);
        assertThat(testClientProject.getProjectDescription()).isEqualTo(UPDATED_PROJECT_DESCRIPTION);
        assertThat(testClientProject.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingClientProject() throws Exception {
        int databaseSizeBeforeUpdate = clientProjectRepository.findAll().size();

        // Create the ClientProject

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientProjectMockMvc.perform(put("/api/client-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProject)))
            .andExpect(status().isCreated());

        // Validate the ClientProject in the database
        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientProject() throws Exception {
        // Initialize the database
        clientProjectService.save(clientProject);

        int databaseSizeBeforeDelete = clientProjectRepository.findAll().size();

        // Get the clientProject
        restClientProjectMockMvc.perform(delete("/api/client-projects/{id}", clientProject.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientProject> clientProjectList = clientProjectRepository.findAll();
        assertThat(clientProjectList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
