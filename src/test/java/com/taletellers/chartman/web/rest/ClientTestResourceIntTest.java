package com.taletellers.chartman.web.rest;

import com.taletellers.chartman.ChartmanApp;

import com.taletellers.chartman.domain.ClientTest;
import com.taletellers.chartman.repository.ClientTestRepository;
import com.taletellers.chartman.service.ClientTestService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.taletellers.chartman.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientTestResource REST controller.
 *
 * @see ClientTestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChartmanApp.class)
public class ClientTestResourceIntTest {

    private static final String DEFAULT_TEST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TEST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TEST_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_TEST_DESCRIPTION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TEST_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TEST_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Inject
    private ClientTestRepository clientTestRepository;

    @Inject
    private ClientTestService clientTestService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restClientTestMockMvc;

    private ClientTest clientTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClientTestResource clientTestResource = new ClientTestResource();
        ReflectionTestUtils.setField(clientTestResource, "clientTestService", clientTestService);
        this.restClientTestMockMvc = MockMvcBuilders.standaloneSetup(clientTestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientTest createEntity(EntityManager em) {
        ClientTest clientTest = new ClientTest()
                .testName(DEFAULT_TEST_NAME)
                .testDescription(DEFAULT_TEST_DESCRIPTION)
                .testDate(DEFAULT_TEST_DATE)
                .active(DEFAULT_ACTIVE);
        return clientTest;
    }

    @Before
    public void initTest() {
        clientTest = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientTest() throws Exception {
        int databaseSizeBeforeCreate = clientTestRepository.findAll().size();

        // Create the ClientTest

        restClientTestMockMvc.perform(post("/api/client-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTest)))
            .andExpect(status().isCreated());

        // Validate the ClientTest in the database
        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeCreate + 1);
        ClientTest testClientTest = clientTestList.get(clientTestList.size() - 1);
        assertThat(testClientTest.getTestName()).isEqualTo(DEFAULT_TEST_NAME);
        assertThat(testClientTest.getTestDescription()).isEqualTo(DEFAULT_TEST_DESCRIPTION);
        assertThat(testClientTest.getTestDate()).isEqualTo(DEFAULT_TEST_DATE);
        assertThat(testClientTest.isActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createClientTestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientTestRepository.findAll().size();

        // Create the ClientTest with an existing ID
        ClientTest existingClientTest = new ClientTest();
        existingClientTest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientTestMockMvc.perform(post("/api/client-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingClientTest)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTestNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientTestRepository.findAll().size();
        // set the field null
        clientTest.setTestName(null);

        // Create the ClientTest, which fails.

        restClientTestMockMvc.perform(post("/api/client-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTest)))
            .andExpect(status().isBadRequest());

        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientTests() throws Exception {
        // Initialize the database
        clientTestRepository.saveAndFlush(clientTest);

        // Get all the clientTestList
        restClientTestMockMvc.perform(get("/api/client-tests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].testName").value(hasItem(DEFAULT_TEST_NAME.toString())))
            .andExpect(jsonPath("$.[*].testDescription").value(hasItem(DEFAULT_TEST_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].testDate").value(hasItem(sameInstant(DEFAULT_TEST_DATE))))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getClientTest() throws Exception {
        // Initialize the database
        clientTestRepository.saveAndFlush(clientTest);

        // Get the clientTest
        restClientTestMockMvc.perform(get("/api/client-tests/{id}", clientTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientTest.getId().intValue()))
            .andExpect(jsonPath("$.testName").value(DEFAULT_TEST_NAME.toString()))
            .andExpect(jsonPath("$.testDescription").value(DEFAULT_TEST_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.testDate").value(sameInstant(DEFAULT_TEST_DATE)))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClientTest() throws Exception {
        // Get the clientTest
        restClientTestMockMvc.perform(get("/api/client-tests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientTest() throws Exception {
        // Initialize the database
        clientTestService.save(clientTest);

        int databaseSizeBeforeUpdate = clientTestRepository.findAll().size();

        // Update the clientTest
        ClientTest updatedClientTest = clientTestRepository.findOne(clientTest.getId());
        updatedClientTest
                .testName(UPDATED_TEST_NAME)
                .testDescription(UPDATED_TEST_DESCRIPTION)
                .testDate(UPDATED_TEST_DATE)
                .active(UPDATED_ACTIVE);

        restClientTestMockMvc.perform(put("/api/client-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientTest)))
            .andExpect(status().isOk());

        // Validate the ClientTest in the database
        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeUpdate);
        ClientTest testClientTest = clientTestList.get(clientTestList.size() - 1);
        assertThat(testClientTest.getTestName()).isEqualTo(UPDATED_TEST_NAME);
        assertThat(testClientTest.getTestDescription()).isEqualTo(UPDATED_TEST_DESCRIPTION);
        assertThat(testClientTest.getTestDate()).isEqualTo(UPDATED_TEST_DATE);
        assertThat(testClientTest.isActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingClientTest() throws Exception {
        int databaseSizeBeforeUpdate = clientTestRepository.findAll().size();

        // Create the ClientTest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientTestMockMvc.perform(put("/api/client-tests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientTest)))
            .andExpect(status().isCreated());

        // Validate the ClientTest in the database
        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientTest() throws Exception {
        // Initialize the database
        clientTestService.save(clientTest);

        int databaseSizeBeforeDelete = clientTestRepository.findAll().size();

        // Get the clientTest
        restClientTestMockMvc.perform(delete("/api/client-tests/{id}", clientTest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientTest> clientTestList = clientTestRepository.findAll();
        assertThat(clientTestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
